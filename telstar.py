import tornado
import argparse
import configparser
import os.path
import telstar
import os
import json
import logging

logger = telstar.getlogger('telstar')

k_prod_settings_file = "telstar.json"
k_dev_settings_file = "telstar-dev.json"
k_min_cookie_secret_length = 16

v_parser = argparse.ArgumentParser()
v_parser.add_argument("application", type=str, help="specify the application to run e.g. 'server', 'api' or 'cron'", action="store")
v_parser.add_argument("option", nargs='?', type=str, help="specify any application specific options", default=None, action="store")
v_parser.add_argument("--port",  type=int, help="port to listen on", action="store")

# TODO All optional parameters and settings should be equakl partners such that they can all be specified in the settings file,
#  command line args and Envorionment vars.

# Optional for server and api
v_parser.add_argument("--init", help="run all @init decorated plugin methods", action="store_true")
v_parser.add_argument("--dev", help="use the telstar-dev.json settings file instead of telstar,json", action="store_true")

# Optional for server only
v_parser.add_argument("--install-example", help="run all @init decorated plugin methods", action="store_true")

# Optional for API only
#v_parser.add_argument("--url-prefix", help="url prefix used for the api server. default="", action="store")
#v_parser.add_argument ("--cookie-secret", help="secret key used to ensure secure cookies are actually secure", action="store", type=str)

# Settings These apply to all apps
v_parser.add_argument("--server", type=str, help="override the server display name setting", default=None, action="store")
v_parser.add_argument("--dbcon", type=str, help="override the connection string setting", default=None, action="store")
v_parser.add_argument("--dbcollection", type=str, help="override the primary/secondary database setting", default=None, action="store")
v_parser.add_argument("--start-page", type=int, help="override the start page setting", default=None, action="store")
v_parser.add_argument("--login-page", type=int, help="override the login page setting", default=None, action="store")
v_parser.add_argument("--main-index_page", type=int, help="override the main index page setting", default=None, action="store")
v_parser.add_argument("--requires-authentication", type=bool, help="override the authenticate setting to require users to be authenticated", action="store", default=None)
v_parser.add_argument("--system-message", help=" override the system message setting", default=None, action="store")
v_parser.add_argument("--parity", type=bool, help="override the parity setting to use seven bit even parity", action="store", default=None)
v_parser.add_argument("--volume-directory", help="override the directory setting to use for plugins", default=None, action="store")
v_parser.add_argument("--hide-pageid", type=bool, help="prevent the page id from being displayed in the frame header", action="store", default=None)
v_parser.add_argument("--hide-navigation-message", type=bool, help="prevent the page id from being displayed in the frame header", action="store", default=None)
v_parser.add_argument("--navigation-row", type=int, help=" override the row on which the navigation should be displayed", default=None, action="store")

v_opts = v_parser.parse_args()

def get_environment_variable(name: str, force_uppercase = True):

    result = ''
    try:
            result = (os.environ[name]).strip()
    except:
        pass

    if force_uppercase:
        return result.upper()

    return result

def update_settings(settings: dict, args: dict = {}):


    # update settings from specifically named environment variables if present
    # however, these can be overridden with any values in args
    # typically args would come from the command line arguments
    # note that if the env var doesn't exist then an exception is thrown
    # which is ignored and the settings file is not changed
    try:
        if args['server'] is not None:
            settings['server'] = args['server']
        else:
            settings['server'] = (os.environ['TELSTAR_SERVER']).upper().strip()
    except:
        pass
    try:
        if args['dbcon'] is not None:
            settings['dbcon'] = args['dbcon']
        else:
            settings['dbcon'] = (os.environ['TELSTAR_DBCON']).lower().strip()
    except:
        pass
    try:
        if args['dbcollection'] is not None:
            settings['dbcollection'] = args['dbcollection']
        else:
            settings['dbcollection'] = (os.environ['TELSTAR_DBCOLLECTION']).lower().strip()
    except:
        pass
    try:
        if args['start_page'] is not None:
            settings['start_page'] = args['start_page']
        else:
            settings['start_page'] = int((os.environ['TELSTAR_START_PAGE']))
    except:
        pass
    try:
        if args['login_page'] is not None:
            settings['login_page'] = args['login_page']
        else:
            settings['login_page'] = int((os.environ['TELSTAR_LOGIN_PAGE']))
    except:
        pass
    try:
        if args['main_index_page'] is not None:
            settings['main_index_page'] = args['main_index_page']
        else:
            settings['main_index_page'] = int((os.environ['TELSTAR_MAIN_INDEX_PAGE']))
    except:
        pass
    try:
        if args['requires_authentication'] is not None:
            settings['requires_authentication'] = args['requires_authentication']
        else:
            settings['requires_authentication'] = (os.environ['TELSTAR_REQUIRES_AUTHENTICATION']).lower().strip() == "true"
    except:
        pass
    try:
        if args['system_message'] is not None:
            settings['system_message'] = args['system_message']
        else:
            settings['system_message'] = telstar.frame_document.decode_markup((os.environ['TELSTAR_SYSTEM_MESSAGE']).strip())
    except:
        pass
    try:
        if args['parity'] is not None:
            settings['parity'] = args['parity']
        else:
            settings['parity'] = (os.environ['TELSTAR_PARITY']).lower().strip() == "true"
    except:
        pass
    try:
        if args['volume_directory'] is not None:
            settings['volume_directory'] = args['volume_directory']
        else:
            settings['volume_directory'] = (os.environ['TELSTAR_VOLUME_DIRECTORY']).lower().strip()
    except:
        pass

    try:
        if args['hide_pageid'] is not None:
            settings['hide_pageid'] = args['hide_pageid']
        else:
            settings['hide_pageid'] = (os.environ['TELSTAR_HIDE_PAGEID']).lower().strip() == "true"
    except:
        pass

    try:
        if args['hide_navigation_message'] is not None:
            settings['hide_navigation_message'] = args['hide_navigation_message']
        else:
            settings['hide_navigation_message'] = (os.environ['TELSTAR_HIDE_NAVIGATION_MESSAGE']).lower().strip()
    except:
        pass

    try:
        if args['navigation_row'] is not None:
            settings['navigation_row'] = args['navigation_row']
        else:
            settings['navigation_row'] = int((os.environ['TELSTAR_NAVIGATION_ROW']).lower().strip())
    except:
        pass

    # undocumented parameter used for development only
    # added to settings for convenience
    try:
        settings['dev'] = args['dev']
    except:
        pass


def response_frame_processor(args):

    frame = args.frame
    plugin_directory = settings["volume_directory"] + "plugins/"

    # check for the plugin and execute it passing the args
    # this executes all methods in all plugings that are decorated
    # with the @responseprocessor decorator
    pl = telstar.PluginLoader(plugin_directory)
    pl.run_plugin_with_decorator("response", settings, args)


if __name__ == '__main__':

    ###############################################################################
    # Determine base path and switch. Ports above 1000 are used for development.
    ###############################################################################
    if v_opts.dev:
        # Development
        base_path = os.path.abspath("./")
        settings_file = k_dev_settings_file
    else:
        # Production Server
        base_path = os.path.abspath("/opt/telstar/")
        settings_file = k_prod_settings_file

    volume_path = base_path + "/volume/"

    os.chdir(base_path)

    ###############################################################################
    # determine settings file
    ###############################################################################

    vol_settings_exists = os.path.exists("{0}{1}".format(volume_path, settings_file))

    if vol_settings_exists:
        # copy it to the base folder
        os.system("cp {0}{1} {2}/{1}".format(volume_path, settings_file, base_path))
    else:
        # if the settings file does not exist in the volume, copy from the base path
        os.system("cp {0}/{1} {2}{1}".format(base_path, settings_file, volume_path))

    path_to_config_file = "{0}/{1}".format(base_path, settings_file)

    global settings
    settings = telstar.Config(path_to_config_file).settings

    # update settings from the environment variables if set
    logger.info("Initial Settings: {0}".format(settings))
    update_settings(settings, vars(v_opts))
    logger.info("Aggregated Settings: {0}".format(settings))

    # create the plugin loader
    pl = telstar.PluginLoader(settings["volume_directory"] + "plugins/")

    if v_opts.application == "cron":
        ###############################################################################
        # Launch any cron plugins
        ###############################################################################
        logger.info("Starting the telstar cron process.")
        logger.info("Running all 'cron' plugin methods.")
        pl.run_plugin_with_decorator("cron", settings, v_opts.option)

    elif v_opts.application == "api":

        if v_opts.port is None:
            print("ERROR: Port must be specified for 'api' application,")
            exit(2)
        try:
            cookie_secret = get_environment_variable('TELSTAR_COOKIE_SECRET', False)
            if len(cookie_secret) <  k_min_cookie_secret_length:
                raise telstar.InvalidCookieSecretError("Cookie secret must be {0}} characters or greater.".format(k_min_cookie_secret_length))
        except:
            logger.exception("Cookie secret not specified.")
            exit(2)

        try:
            if v_opts.dev:
                telstar_apiuserid = "0"
                telstar_apipassword = "telstarapisecret"
            else:
                get_environment_variable('')
                telstar_apiuserid  = get_environment_variable('TELSTAR_API_USERID')
                telstar_apipassword = get_environment_variable('TELSTAR_API_PASSWORD', False)
                logger.info("API admin user not added, use --init parameter to the command line to create admin user.")

            if v_opts.init and len(telstar_apiuserid) > 0 and len(telstar_apipassword) > 0 :

                auth = telstar.Auth(settings["dbcon"])
                if (auth.insert_user(int(telstar_apiuserid), telstar_apipassword)) is None:
                    logger.error("Unable to create admin user, check that the password supplied is valid.")
                else:
                    logger.info("API admin user added.")

        except:
            logger.warning("Unable to add API user, check that the user ID (int) and password (str) have been specified.")

        ###############################################################################
        # Launch the api server
        ###############################################################################
        logger.info("Starting the telstar api server.")
        url_prefix = get_environment_variable('TELSTAR_API_URL_PREFIX')
        if len(url_prefix) > 0:
            url_prefix = url_prefix.strip("/")

        web_server = telstar.WebServer(v_opts.port, settings, url_prefix)
        web_server.listen(cookie_secret)

    elif v_opts.application == "server":

        if v_opts.port is None:
            print("ERROR: Port must be specified for 'server' application.")
            exit(2)

        ###############################################################################
        # Launch the videotex server
        ###############################################################################
        logger.info("Starting the telstar videotex server.")

        if v_opts.install_example:

            setup = telstar.Install(settings)
            setup.initialise_db()

            logger.info("Running all 'install' plugin methods.")
            pl.run_plugin_with_decorator("install", settings)

        if v_opts.init:
            logger.info("Running all 'init' plugin methods.")
            pl.run_plugin_with_decorator("init", settings)

        pl.run_plugin_with_decorator("server", settings)

        logger.info("Starting the telstar videotex server on port.")
        server = telstar.Server(v_opts.port, settings)
        server.response_complete_event += response_frame_processor
        server.listen(v_opts.port)
        server.io_loop = telstar.IOLoop.current()
        server.io_loop.start()

    else:
        logger.error("Application not recognised.")

