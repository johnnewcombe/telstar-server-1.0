from unittest import TestCase
from tests.helpers import *
import json
import requests

k_api_url = "http://localhost:25232"

class TestUserHandler(TestCase):

    helper = TestHelper()

    def setUp(self):

        self.helper.setUp()

    def tearDown(self):

        self.helper.tearDown()

    def _authenticate(self, username, password):

        payload = json.dumps({"un": username, "pw": password})
        headers = {
            'content-type': 'Content-Type: application/json'
        }
        response = requests.put(k_api_url + "/login", headers=headers, data=payload)
        self.cookies = response.cookies
        self.assertEqual(200, response.status_code)
        print(response.text)

    def test_get_as_admin(self):

        # logon as admin
        self._authenticate(0, "telstarapisecret" )

        # Retrieve a Frame from TELSTAR as admin
        response = requests.get("{0}/frame/{1}{2}".format(k_api_url, 101, "a"), cookies=self.cookies)
        data = json.loads(response.text)
        self.assertEqual(200, response.status_code)
        print(response.text)

    def test_get_as_user_in_bounds_1(self):

        # logon as admin
        self._authenticate(101, "password101" )

        # Retrieve an OUT OF BOUNDS  Frame from TELSTAR as admin
        response = requests.get("{0}/frame/{1}{2}".format(k_api_url, 101, "a"), cookies=self.cookies)
        data = json.loads(response.text)
        self.assertEqual(response.status_code, 200) # OK
        print(response.text)

    def test_get_as_user_in_bounds_2(self):

        self._authenticate(100, "password100")

        payload = json.dumps({"page_no": 1001, "frame_id": "a"})

        headers = {
            'content-type': 'Content-Type: application/json'
        }

        response = requests.get(k_api_url + "/frame", headers=headers, data=payload, cookies=self.cookies)

        self.assertEqual(200, response.status_code)
        print(response.text)

    def test_get_as_user_out_of_bounds(self):

        # logon as admin
        self._authenticate(100, "password100" )

        # Retrieve an OUT OF BOUNDS  Frame from TELSTAR as admin
        response = requests.get("{0}/frame/{1}{2}".format(k_api_url, 101, "a"), cookies=self.cookies)
        data = json.loads(response.text)
        self.assertEqual(response.status_code, 403) # Forbidden
        print(response.text)

    def test_put_as_admin(self):

        self._authenticate(0, "telstarapisecret")

        payload = json.dumps(TestHelper.create_frame_edit_tf(102, "a"))

        headers = {
            'content-type': 'Content-Type: application/json'
        }

        response = requests.put(k_api_url + "/frame", headers=headers, data=payload, cookies=self.cookies)

        self.assertEqual(200, response.status_code)
        print(response.text)

    def test_put_as_user_in_bounds(self):

        self._authenticate(100, "password100")

        payload = json.dumps(TestHelper.create_frame_edit_tf(1001, "a"))

        headers = {
            'content-type': 'Content-Type: application/json'
        }

        response = requests.put(k_api_url + "/frame", headers=headers, data=payload, cookies=self.cookies)

        self.assertEqual(200, response.status_code)
        print(response.text)

    def test_put_as_user_out_of_bounds(self):

        self._authenticate(101, "password101")

        payload = json.dumps(TestHelper.create_frame_edit_tf(1001, "a"))

        headers = {
            'content-type': 'Content-Type: application/json'
        }

        response = requests.put(k_api_url + "/frame", headers=headers, data=payload, cookies=self.cookies)

        self.assertEqual(403, response.status_code) # Forbidden
        print(response.text)

    def test_delete_as_admin(self):

        self._authenticate(0, "telstarapisecret")

        payload = json.dumps({"page_no": 101, "frame_id": "a"})

        headers = {
            'content-type': 'Content-Type: application/json'
        }
        response = requests.delete(k_api_url + "/frame", headers=headers, data=payload, cookies=self.cookies)
        print(response.text)

        self.assertEqual(200, response.status_code)

    def test_delete_as_user_in_bounds_1(self):

        self._authenticate(101, "password101")

        response = requests.delete(k_api_url + "/frame/101a", cookies=self.cookies)
        print(response.text)

        self.assertEqual(200, response.status_code)

    def test_delete_as_user_in_bounds_2(self):

        self._authenticate(101, "password101")

        payload = json.dumps({"page_no": 101, "frame_id": "a"})

        headers = {
            'content-type': 'Content-Type: application/json'
        }
        response = requests.delete(k_api_url + "/frame", headers=headers, data=payload, cookies=self.cookies)
        print(response.text)

        self.assertEqual(200, response.status_code)

    def test_delete_as_user_out_of_bounds(self):

        self._authenticate(100, "password100")

        payload = json.dumps({"page_no": 101, "frame_id": "a"})

        headers = {
            'content-type': 'Content-Type: application/json'
        }
        response = requests.delete(k_api_url + "/frame", headers=headers, data=payload, cookies=self.cookies)
        print(response.text)

        self.assertEqual(403, response.status_code)


if __name__ == '__main__':

    # username = 800800800
    # password = "010266"
    username = 0
    password = "telstarapisecret"

    # live api is accessed vi a ssh tumnnet from the local host port 8082.
    #api_url = "http://localhost:8001/telstar"
    api_url = "http://localhost:25232/telstar"

    # payload = {'key1': 'value1', 'key2': 'value2'}
    # r = requests.get('https://httpbin.org/get', params=payload)

    #Authenticate
    payload = json.dumps({"un": username, "pw": password})
    headers = {
        'content-type': 'Content-Type: application/json'
    }
    response = requests.put(api_url + "/login", headers=headers, data=payload)
    cookies = response.cookies
    print(response.text)

    #############################################################
    # Creating a New Frame, update, retrieve and save to disk.
    page_no = 101
    frame_id = "a"
    #
    # #############################################################
    # # Create a new Frame on TELSTAR
    payload = TestHelper.create_frame_edit_tf(page_no, frame_id)
    headers = {
        'content-type': 'Content-Type: application/json'
    }
    response = requests.put(api_url + "/frame", headers=headers, data=payload, cookies=cookies)
    print(response.text)
    print("{0}: {1}".format(response.status_code, response.reason))
    #
    # Retrieve a Frame from TELSTAR
    response = requests.get("{0}/frame/{1}{2}".format(api_url, page_no,frame_id), cookies=cookies)
    data = json.loads(response.text)
    print(response.text)
    print("{0}: {1}".format(response.status_code, response.reason))

    # Delete a frame
    payload = json.dumps({"page_no": 101, "frame_id": "a"})
    headers = {
        'content-type': 'Content-Type: application/json'
    }
    response = requests.delete(api_url + "/frame", headers=headers, data=payload, cookies=cookies)
    print(response.text)
    print("{0}: {1}".format(response.status_code, response.reason))


    # Add user
    payload = json.dumps({"user_id": 800800800, "password": "telstarapisecret"})
    headers = {
        'content-type': 'Content-Type: application/json'
    }
    response = requests.put(api_url + "/user", headers=headers, data=payload, cookies=cookies)
    print(response.text)
    print("{0}: {1}".format(response.status_code, response.reason))

    # Delete User
    payload = json.dumps({"user_id": 800800800})
    headers = {
        'content-type': 'Content-Type: application/json'
    }
    response = requests.delete(api_url + "/user", headers=headers, data=payload, cookies=cookies)
    print(response.text)
    print("{0}: {1}".format(response.status_code, response.reason))

    #
    # # To access the json as a dictionary, the following can be used
    # frame = response.json()
    #
    # # Save the frame to disk
    # with open("{0}{1}.json".format(page_no,frame_id), "w") as file:
    #     file.write(json_data)
    #
