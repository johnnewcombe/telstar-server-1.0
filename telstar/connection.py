from enum import *
from telstar.log_utils import *

logger = getlogger(__name__)

class ConnectionMode(Enum):

    full_duplex = 0             # Full duplex where gateway service echo's the data.
    full_duplex_no_echo = 1     # Full duplex where gateway service does not echo the data.
    half_duplex = 2             # Line mode where data is terminated by CR and responds in a similar manner. no echo from server
    viewdata = 3                # Full duplex Viewdata service, this will not need buffering.

    @classmethod
    def get_index(cls, type):
        return list(cls).index(type)

class Connection:

    def __init__(self, address, port, connection_mode = ConnectionMode.full_duplex):
        self.address = address
        self.port = port
        self.mode = connection_mode