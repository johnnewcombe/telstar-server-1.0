import configparser
from telstar.dal import *
from telstar.globals import *
from telstar.setup_data import *
from telstar import frame_document
from telstar import setup_data
from telstar.config import *
from telstar.log_utils import *

logger = getlogger(__name__)

import telstar.globals

# Configure logging.
# logger = logging.getLogger(os.path.basename(__file__))
# logger.setLevel(logging.INFO)
# setup_console_handler = logging.StreamHandler()
# logger.addHandler(setup_console_handler)

class Install:

    def __init__(self, settings: dict):

        logger.info("Loading configuration.")

        try:
            self.settings = settings
            self.primary_db = self.settings["dbcollection"] == "primary"

        except Exception as ex:
            logger.exception("Exception: {0} Message: {1}".format(type(ex), str(ex)))

    def initialise_db(self):

        logger.info("Initialising Telstar.")

        try:
            dbcon = self.settings["dbcon"]
            if len(dbcon) == 0:
                raise DatabaseConnectionNotFoundError()

            dal = Dal(dbcon)

            status = dal.get_server_status()
            logger.info("Connected to {0}.".format(status["host"]))
            logger.info("Using {0} collection.".format(self.settings["dbcollection"]))

            self._create_entry_page(dal)
            self._create_exit_page(dal)
            self._create_system_info_page(dal)
            self._create_redirect_pages(dal)
            self._create_static_pages(dal)
            self._create_gateway_pages(dal)
            self._create_frames_response(dal)

        except Exception as ex:
            logger.exception("Exception: {0} Message: {1}".format(type(ex), str(ex)))

    def _create_frames_response(self, dal):
        try:

            page_no = 990
            frame_id = "a"

            logger.info('Creating Logon Page.')

            fields = []
            fields.append(frame_document.create_frame_field(label='City/Town : ',
                                                            horz_position=4,
                                                            vert_position=8,
                                                            required=True,
                                                            length=16,
                                                            field_type=FieldType.numeric,
                                                            auto_submit=True
                                                            ))

            title = '[D]Login\r\n\r\n'

            content = [k_controls['mosaic_blue'] + k_separator_graphics["dots"],
                       frame_document.centre_text('Welcome to the[Y]TELSTAR'),
                       frame_document.centre_text('videotex service.\r\n'),
                       k_controls['mosaic_blue'] + k_separator_graphics["dots"], self._get_blob("logo")]

            routing_table = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

            fields = []
            fields.append(frame_document.create_frame_field(label='Telstar ID : ',
                                                            horz_position=4,
                                                            vert_position=16,
                                                            required=True,
                                                            length=9,
                                                            field_type=FieldType.numeric,
                                                            auto_submit=True,
                                                            password=False
                                                            ))
            fields.append(frame_document.create_frame_field(label='  Password : ',
                                                            horz_position=4,
                                                            vert_position=18,
                                                            required=True,
                                                            length=9,
                                                            field_type=FieldType.numeric,
                                                            auto_submit=True,
                                                            password=True
                                                            ))
            frame_doc = frame_document.create_doc(page_no=page_no, frame_id=frame_id, frame_type=ResponseFrame,
                                                  content_data=content, content_type="markup",
                                                  title_data=title, title_type="markup",
                                                  author_id="response-frame-generator", routing_table=routing_table,
                                                  static_page=True,
                                                  cursor=True, connection_address="", connection_port=0,
                                                  connection_mode="", response_fields=fields
                                                  )

            dal.insert_document(frame_doc, self.primary_db)  # secondary db only


        except Exception as ex:
            logger.exception(ex)

    def _create_entry_page(self, dal):

        page_no = 99
        frame_id = "a"

        logger.info("Creating the Entry page.")

        title = '[D]Welcome\r\n\r\n'
        content = [k_controls['mosaic_blue'] + k_separator_graphics["dots"],
                   frame_document.centre_text('Welcome to the[Y]TELSTAR'),
                   frame_document.centre_text('videotex service.\r\n'),
                   frame_document.centre_text('You are connected to[Y][SERVER].'),
                   k_controls['mosaic_blue'] + k_separator_graphics["dots"], self._get_blob("logo")]

        routing_table = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        frame_doc = frame_document.create_doc(page_no=page_no, frame_id=frame_id, frame_type=InitialFrame,
                                              content_data=content, content_type="markup",
                                              title_data=title, title_type="markup",
                                              author_id="system-frame-generator", routing_table=routing_table,
                                              static_page=True,
                                              cursor=False, connection_address="", connection_port=0,
                                              connection_mode="", response_fields=[])

        dal.insert_document(frame_doc, self.primary_db)  # secondary db only

    def _create_exit_page(self, dal):

        page_no = 90
        frame_id = "a"

        logger.info("Creating the Exit page.")

        title = '[D]Goodbye\r\n\r\n'

        content = [k_controls['mosaic_blue'] + k_separator_graphics["dots"],
                   frame_document.centre_text('Thankyou for using[Y]TELSTAR.'),
                   frame_document.centre_text('You were connected to[Y][SERVER].'),
                   k_controls['mosaic_blue'] + k_separator_graphics["dots"], self._get_blob("logo")]

        # routing_table = frame_document.create_default_routing_table(page_no)
        routing_table = [990, 990, 990, 990, 990, 990, 990, 990, 990, 990, 990]

        frame_doc = frame_document.create_doc(page_no=page_no, frame_id=frame_id, frame_type=ExitFrame,
                                              content_data=content, content_type="markup",
                                              title_data=title, title_type="markup",
                                              author_id="system-frame-generator", routing_table=routing_table,
                                              static_page=True,
                                              cursor=False, connection_address="", connection_port=0,
                                              connection_mode="", response_fields=[])

        dal.insert_document(frame_doc, self.primary_db)

    def _create_system_info_page(self, dal):

        page_no = 93
        frame_id = "a"

        logger.info('Creating the System Info page.')

        title = '[D]System Info\r\n\r\n'

        content = [
            k_controls['mosaic_blue'] + k_separator_graphics["dots"]]  # Remainder of content is added dynamically

        routing_table = [9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9]

        frame_doc = frame_document.create_doc(page_no=page_no, frame_id=frame_id, frame_type=SystemFrame,
                                              content_data=content, content_type="markup",
                                              title_data=title, title_type="markup",
                                              author_id="system-frame-generator", routing_table=routing_table,
                                              static_page=True,
                                              cursor=False, connection_address="", connection_port=0,
                                              connection_mode="", response_fields=[])

        dal.insert_document(frame_doc, self.primary_db)

    def _create_redirect_pages(self, dal):

        logger.info("Creating redirect pages.")
        for frame_doc in k_redirect_pages:
            dal.insert_document(frame_doc, self.primary_db)

    def _create_static_pages(self, dal):

        logger.info("Creating system pages.")
        for key, value in k_system_pages.items():
            page_id = key
            pid = Frame.get_pageid_elements(page_id)
            content = value[0]
            f_type = value[1]

            routing_table = value[2]

            frame_doc = frame_document.create_doc(page_no=pid["page_no"], frame_id=pid["frame_id"], frame_type=f_type,
                                                  content_data=content, content_type="edit.tf",
                                                  title_data="", title_type="", author_id="editf-frame-generator",
                                                  routing_table=routing_table, cursor=False, connection_address="",
                                                  static_page=True,
                                                  connection_port=0, connection_mode="", header_text="")

            dal.insert_document(frame_doc, self.primary_db)

    def _create_gateway_pages(self, dal):

        frame_title = "[D]Gateway Connection\r\n\r\n"
        try:

            logger.info("Creating the Gateway Header pages... ")

            page_no = 601
            frame_id = "a"

            content = [k_controls['mosaic_blue'] + k_separator_graphics["dots"], '[C]You are about to connect to Cave',
                       '[C]Adventure, make sure you have a map!\r\n\r\n', '[Y]To disconnect, use[Y]+++_.\r\n\r\n']

            routing_table = frame_document.create_default_routing_table(page_no)
            routing_update = [6, None, None, None, None, None, None, None, None, None, None]

            # update the default routing table if a new one is specified
            for i in range(len(routing_table)):
                if routing_update[i] is not None:
                    routing_table[i] = routing_update[i]

            frame_doc = frame_document.create_doc(page_no=page_no, frame_id=frame_id, frame_type=GatewayFrame,
                                                  content_data=content, content_type="markup",
                                                  title_data=frame_title,
                                                  title_type="markup", author_id="gateway-frame-generator",
                                                  routing_table=routing_table,
                                                  static_page=True,
                                                  cursor=False, connection_address="adventure-server",
                                                  connection_port=6505, connection_mode="full_duplex_no_echo"
                                                  )

            primary = self.settings["dbcollection"] == "primary"
            result = dal.insert_document(frame_doc, primary)

            # JOHN NEWCOMBE, FFEDFFEC
            page_no = 602
            frame_id = "a"

            content = [k_controls['mosaic_blue'] + k_separator_graphics["dots"],
                       '[C]You are about to connect to[M]CCL4.', '[C]Prepare yourself for Viz-type humour.\r\n\r\n',
                       '[M]To disconnect, use[Y]+++_.\r\n\r\n']

            routing_table = frame_document.create_default_routing_table(page_no)
            routing_update = [6, None, None, None, None, None, None, None, None, None, None]

            # update the default routing table if a new one is specified
            for i in range(len(routing_table)):
                if routing_update[i] is not None:
                    routing_table[i] = routing_update[i]

            frame_doc = frame_document.create_doc(page_no=page_no, frame_id=frame_id, frame_type=GatewayFrame,
                                                  content_data=content, content_type="markup",
                                                  title_data=frame_title,
                                                  title_type="markup", author_id="gateway-frame-generator",
                                                  routing_table=routing_table, cursor=False,
                                                  static_page=True,
                                                  connection_address="fish.ccl4.org", connection_port=23,
                                                  connection_mode="viewdata"
                                                  )
            primary = self.settings["dbcollection"] == "primary"
            result = dal.insert_document(frame_doc, primary)

            page_no = 603
            frame_id = "a"

            content = [k_controls['mosaic_blue'] + k_separator_graphics["dots"],
                       '[C]You are about to connect to the[Y]NXTEL',
                       '[C]videotex system for the[Y]ZX Spectrum.\r\n\r\n', '[M]To disconnect, use[Y]+++_.\r\n\r\n']

            routing_table = frame_document.create_default_routing_table(page_no)
            routing_update = [6, None, None, None, None, None, None, None, None, None, None]

            # update the default routing table if a new one is specified
            for i in range(len(routing_table)):
                if routing_update[i] is not None:
                    routing_table[i] = routing_update[i]

            frame_doc = frame_document.create_doc(page_no=page_no, frame_id=frame_id, frame_type=GatewayFrame,
                                                  content_data=content, content_type="markup",
                                                  title_data=frame_title, title_type="markup",
                                                  author_id="gateway-frame-generator", routing_table=routing_table,
                                                  static_page=True,
                                                  cursor=False, connection_address="nx.nxtel.org",
                                                  connection_port=23280, connection_mode="viewdata",
                                                  )

            primary = self.settings["dbcollection"] == "primary"
            result = dal.insert_document(frame_doc, primary)

            page_no = 604
            frame_id = "a"

            content = [k_controls['mosaic_blue'] + k_separator_graphics["dots"], '[C]You are about to connect to the',
                       '[C]TeeFax[Y]Teletext system.\r\n\r\n', '[C]To disconnect, use[Y]+++_.\r\n\r\n']

            routing_table = frame_document.create_default_routing_table(page_no)
            routing_update = [6, None, None, None, None, None, None, None, None, None, None]

            # update the default routing table if a new one is specified
            for i in range(len(routing_table)):
                if routing_update[i] is not None:
                    routing_table[i] = routing_update[i]

            frame_doc = frame_document.create_doc(page_no=page_no, frame_id=frame_id, frame_type=GatewayFrame,
                                                  content_data=content, content_type="markup",
                                                  title_data=frame_title,
                                                  title_type="markup", author_id="gateway-frame-generator",
                                                  routing_table=routing_table, cursor=False,
                                                  static_page=True,
                                                  connection_address="pegasus.matrixnetwork.co.uk",
                                                  connection_port=6502,
                                                  connection_mode="viewdata"
                                                  )

            primary = self.settings["dbcollection"] == "primary"
            result = dal.insert_document(frame_doc, primary)

        except Exception as ex:
            logger.exception(ex)

    def _get_blob(self, name: str):

        # import the data from setup_data.py
        data_import = EditTfImport()
        data = setup_data.k_system_blobs[name]
        data_import.data = data[0]

        # create the blob
        return data_import.import_data(data[1], data[2], data[3], data[4], False)
