
k_re_page_id = '^[0-9]+[a-z]$' # e.g. 101a
k_re_page_no = '^[0-9]+$'      # e.g. 101

k_separator_graphics = {"dots": "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$",
                        "solid": ",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,"}
k_controls = {
    'alpha_red': '\x1b\x41',
    'alpha_green': '\x1b\x42',
    'alpha_yellow': '\x1b\x43',
    'alpha_blue': '\x1b\x44',
    'alpha_magenta': '\x1b\x45',
    'alpha_cyan': '\x1b\x46',
    'alpha_white': '\x1b\x47',
    'flash': '\x1b\x48',
    'steady': '\x1b\x49',
    'normal_height': '\x1b\x4c',
    'double_height': '\x1b\x4d',
    'black_background': '\x1b\x5c',
    'new_background': '\x1b\x5d',
    'mosaic_red': '\x1b\x51',
    'mosaic_green': '\x1b\x52',
    'mosaic_yellow': '\x1b\x53',
    'mosaic_blue': '\x1b\x54',
    'mosaic_magenta': '\x1b\x55',
    'mosaic_cyan': '\x1b\x56',
    'mosaic_white': '\x1b\x57',
    'hash': '\x5f',
}

# These are only used if the setting does not exist in the user settings file.
k_default_settings = {
    'server': 'MYSERVER',
    'dbcollection': 'secondary',
    'dbcon': '',
    'start_page': 99,
    'login_page': 990,
    'main_index_page': 0,
    'requires_authentication': False,
    'system_message': 'Development System',
    'parity': False,
    'version':'0.1',
    'cugs': [8008,900]
}
