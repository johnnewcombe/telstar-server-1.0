import datetime
from telstar.log_utils import *

logger = getlogger(__name__)


class Cache:
    """"""
    def __init__(self, cache_minutes: int = 30, max_cache_size: int = 1000):
        """Constructor"""
        self._cache = {}
        self.cache_minutes = cache_minutes
        self.max_cache_size = max_cache_size

    def __contains__(self, key):
        """
        Returns True or False depending on whether or not the key is in the
        _cache
        """
        return key in self._cache

    def get(self, key: str):

        result = None

        if key in self._cache:

            # TODO check for timeout against self.cache_minutes
            date_accessed = self._cache[key]["date_accessed"]

            minutes_diff = (datetime.datetime.now() - date_accessed).total_seconds() / 60.0

            if minutes_diff < self.cache_minutes:
                # if timeout OK then update
                value = self._cache[key]["value"]
                self.update(key, value)

                result =  value
            else:
                # remove key as it is now invalid
                del(self._cache[key])

        return result

    def update(self, key, value):
        """
        Update the _cache dictionary and optionally remove the oldest item
        """
        if key not in self._cache and len(self._cache) >= self.max_cache_size:
            self.remove_oldest()
        self._cache[key] = {'date_accessed': datetime.datetime.now(),
                           'value': value}

    def remove_oldest(self):
        """
        Remove the entry that has the oldest accessed date
        """
        oldest_entry = None
        for key in self._cache:
            if oldest_entry is None:
                oldest_entry = key
            elif self._cache[key]['date_accessed'] < self._cache[oldest_entry][
                'date_accessed']:
                oldest_entry = key
        self._cache.pop(oldest_entry)

    @property
    def size(self):
        """
        Return the size of the _cache
        """
        return len(self._cache)
