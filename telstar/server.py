# Viewdata server
# Copyright John Newcombe
#
# TODO character mode needs to be able to interrupt the rendering '* #' mode is line based
# TODO Check what is normally displayed in the sys messages field

import struct
import threading

from tornado.tcpserver import TCPServer
from tornado.ioloop import * # <-- NEEDED
from telstar.dal import *
from telstar.telnet_parser import *
from telstar.config import *
from telstar.event import *
from telstar.server_process_routing import *
from telstar.server_process_gateway import *
from telstar.globals import *
from telstar.log_utils import *

logger = getlogger(__name__)

# Cache this struct definition; important optimization.
int_struct = struct.Struct("<i")
_UNPACK_INT = int_struct.unpack
_PACK_INT = int_struct.pack

# delay factor
k_baud_4800 = 0.002083
k_baud_2400 = 0.004167
k_baud_1200 = 0.008333
k_baud_max = 0.0

k_hashGuardTime = 0.2 # 200ms


class Server(TCPServer):

    def __init__(self, port: int, settings: dict, update_secs: int = 14400):

        TCPServer.__init__(self)
        self.update_secs = update_secs
        self.response_complete_event = Event()
        self.update_event = Event()

        self.port = port
        self.settings = settings
        self._logon_attempts = 0

    async def handle_stream(self, stream, address):

        stream.set_close_callback(self.stream_close_callback)
        cancel_event = None

        logger.info("Connection from client address {0}.".format(address))

        telnet_parser = TelnetParser(stream)

        # logger.info('Sending DO LINE MODE Telnet negotiation (all clients)')
        # this sets the telnet client to char mode rather than line mode
        # must use native stream write not the wrapper in the Frame object
        # removed for now
        # send DO LINE MODE to every caller, if the caller is a TELNET client it will respond with a WILL LINE MODE,
        # the Telnet Parser will detect this and set the baud rate appropriately.
        # stream.write(b'\xFF\xFD\x22\xFF\xFB\x01')

        # reset all the object store, this is used to pass and gateway
        # object to and from the 'process_stream' future
        dbcon = self.settings["dbcon"]
        if len(dbcon) == 0:
            raise DatabaseConnectionNotFoundError()

        dal = Dal(dbcon)
        routing = Routing(self.settings, dal)
        object_args = [None, None, None, routing]
        last_char_received = 0
        while True:

            try:

                char = await stream.read_bytes(1)
                asc = ord(char)

                now = datetime.datetime.now().timestamp()
                if asc == 0x5f and routing.immediate_mode:
                    if last_char_received >= now - k_hashGuardTime:
                        logger.info('Byte {0} ({1}) was received within %dms of previous character whilst in immediate mode.'.format(hex(asc), asc, k_hashGuardTime))
                        continue


                last_char_received = datetime.datetime.now().timestamp()

                logger.info('Byte Received {0} ({1})'.format(hex(asc), asc))

                # See above
                char = telnet_parser.parse(char)

                if telnet_parser.telnet_connection:
                    logger.info('Telnet client, Baud rate is maximum value.')
                    baud_rate = k_baud_max

                else:
                    logger.info('Baud rate is 1200 baud.')
                    # slow down the baud rate if not a telnet connection.
                    baud_rate = k_baud_1200

                if len(char) > 0:

                    # when received client input, cancel running job, this simply signals
                    # an event and allows the 'process_stream' future to shut down gracefully
                    if cancel_event:
                        cancel_event.set()

                    cancel_event = threading.Event()

                    # Remove parity
                    char = self.remove_parity(char)
                    asc = ord(char)

                    logger.info('Byte to be processed {0} ({1})'.format(hex(asc), asc))

                    gateway = object_args[1]

                    # set the current frame to None if not authenticated, this will ensure that the
                    # initial/login page is shown
                    # if self.settings["authenticate"]=='True' and not self._authenticated:
                    #     object_args[0] = None

                    # could be routing normally to display local pages etc. or could be a proxy to a gateway service


                    if gateway is not None and gateway.connected:

                        server_process = ServerProcessGateway(self.settings)

                        # Converts the 'process_stream' method defined above (yielded object) into a Future
                        process_gateway_future = gen.convert_yielded(server_process.process(char, stream,
                                                                                          baud_rate,
                                                                                          cancel_event,
                                                                                          object_args))

                        # Schedules a callback on the IOLoop when the given Future is finished.
                        self.io_loop.add_future(process_gateway_future, self.gateway_processed_callback)

                    else:

                        server_process = ServerProcessRouting(self.settings)
                        server_process.response_complete_event = self.response_complete_event

                        # Converts the 'process_stream' method defined above (yielded object) into a Future
                        process_routing_future = gen.convert_yielded(server_process.process(char, stream,
                                                                                            baud_rate,
                                                                                            cancel_event,
                                                                                            object_args))

                        # Schedules a callback on the IOLoop when the given Future is finished.
                        self.io_loop.add_future(process_routing_future, self.routing_processed_callback)

            except StreamClosedError as ex:
                logger.info("{0} Disconnected: {1} Message: {2}".format(address, type(ex), str(ex)))
                break;
            except Exception as ex:
                logger.exception("{0} Exception: {1} Message: {2}".format(address, type(ex), str(ex)))
                break

    def routing_processed_callback(*args, **kwargs):
        # logger.info('Render callback complete (args=%r, kwargs=%r)' % (args, kwargs))
        logger.info('Routing process callback complete.')

    def gateway_processed_callback(*args, **kwargs):
        # logger.info('Render callback complete (args=%r, kwargs=%r)' % (args, kwargs))
        logger.info('Gateway process callback complete.')

    def stream_close_callback(self):
        """
        This mostly is not necessary for applications that use the Future interface; all outstanding
        Futures will resolve with a StreamClosedError when the stream is closed. However, it is still
        useful as a way to signal that the stream has been closed while no other read or write is in
        progress.
        :return:
        """
        logger.info('Stream close callback complete.')

    @staticmethod
    def remove_parity(char):

        ascii = ord(char)
        ascii &= 127

        return chr(ascii)
