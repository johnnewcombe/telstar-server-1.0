import re
import math
from telstar.frame_field import *
from telstar.alphagraphics import *
from telstar.globals import *
from telstar.log_utils import *

logger = getlogger(__name__)

# These constants only apply to this module so not in global_constants
#k_colours = ["yellow", "red", "blue", "majenta", "cyan", "white", "green"]
k_frame_type_names = {"initial": InitialFrame, "mainindex": MainIndexFrame,
                      "information": InformationFrame, "exit": ExitFrame,
                      "gateway": GatewayFrame, "interactive": InteractiveFrame,
                      "test": TestFrame, "telesoftware": TelesoftwareFrame,
                      "response": ResponseFrame, "system": SystemFrame,
                      "exception": ExceptionFrame}


def doc_to_frame(frame_doc: dict):

    """
    This function creates a frame object based on the specified dictionary.
    :param frame_doc:
    :param settings:
    :return:
    """
    frame = None

    # entered into the database could crash the system
    if frame_doc is not None:

        if "_id" in frame_doc:
            del frame_doc["_id"]

        # create frame, add content and save
        if "pid" in frame_doc and "page-no" in frame_doc["pid"] and "frame-id" in frame_doc["pid"]:

            page_id = str(frame_doc["pid"]["page-no"]) + frame_doc["pid"]["frame-id"]
            visible = frame_doc["visible"] if "visible" in frame_doc else False
            frame_type = frame_doc["frame-type"] if "frame-type" in frame_doc else "information"

            frame = _create_frame(frame_type, page_id)

            # sort out content and title
            if "content" in frame_doc and "data" in frame_doc["content"]:

                content = frame_doc["content"]["data"]

                if "type" in frame_doc["content"]:
                    content_type = frame_doc["content"]["type"]
                else:
                    content_type = "raw"

                # convert content as appropriate
                if content_type == "edit.tf":

                    data_import = EditTfImport()
                    data_import.data = content

                    # take all 23 lines for test pages Row 0 used for header.
                    row_end = 23 if frame_type == "test" else 22

                    frame_doc["content"]["data"] = data_import.import_data(1, row_end)

                elif content_type == "markup":

                    raw_content = ""
                    if type(content) is list:
                        for para in content:
                            raw_content += decode_markup(para) + "\r\n"
                        frame_doc["content"]["data"] = raw_content

                    else:
                        frame_doc["content"]["data"] = decode_markup(content)

                #elif content_type == "raw":
                else: # it is treated as raw
                    if type(content) is bytes:
                        frame_doc["content"]["data"] = content.decode("utf-8")
                    else:
                        frame_doc["content"]["data"] = content
            else:
                frame_doc["content"] = {"data": "\r\n\r\n{0}Welcome to TELSTAR!\r\n".format(k_controls["alpha_yellow"])}

            if "title" in frame_doc and "data" in frame_doc["title"]:

                #and "type" in frame_doc["title"]:
                # convert title
                title = frame_doc["title"]["data"]

                if "type" in frame_doc["title"]:
                    title_type = frame_doc["title"]["type"]  # no autodetection with title
                else:
                    title_type = "raw"

                if "header-text" in frame_doc:
                    frame_doc["header-text"] = decode_markup(frame_doc["header-text"])

                if len(title) > 0:

                    if title_type == "markup":

                        frame_doc["title"]["data"] = decode_markup(title)

                    elif title_type.startswith("alpha-graphic"):

                        # get the colour
                        colour = ""
                        elements = title_type.split("-")
                        if len(elements) == 3:
                            colour = "mosaic_" + elements[2]

                        if colour in k_controls:
                            col_ctrl = k_controls[colour]
                        else:
                            col_ctrl = k_controls["mosaic_white"]

                        ag = Alphagraphics()
                        ag_string = ag.create_alphagraphic_string(frame_doc["title"]["data"])

                        frame_doc["title"]["data"] = ag_string.to_blob(col_ctrl, 0, 0)

                    else: # it is treated as raw
                        pass

            # the frame can be loaded using json the additional fields in the dict/json will be ignored
            # so it can be passed as is
        frame.load(json.dumps(frame_doc))

    return frame


def create_doc(page_no: int, frame_id: chr, frame_type: type,
               content_data, # No Type hint as we could get a list or a str
               content_type: str, title_data: str, title_type: str,
               author_id: str, routing_table: list, static_page: bool, cursor: bool = False,
               connection_address: str = "", connection_port: int = 0,
               connection_mode: str = "", response_fields: list = [],
               nav_msg_forecolour: str = "yellow", nav_msg_backcolour: str = "blue",
               nav_msg_highlight: str = "white",
               header_text="[G]T[R]E[C]L[B]S[W]T[M]A[Y]R",
               visible=True):
    """
    :param page_no:
    :param frame_id:
    :param frame_type:
    :param content_data:
    :param content_type:
    :param title_data:
    :param title_type:
    :param title_colour:
    :param author_id:
    :param routing_table:
    :param static_page:
    :param cursor:
    :param connection_address:
    :param connection_port:
    :param connection_mode:
    :param response_fields:
    :return:
    """

    for key, value in k_frame_type_names.items():
        if value == frame_type:
            f_type = key
            break

    if len(routing_table) != 11:
        raise DocumentDataError("The routing table is an invalid size.")

    for i in routing_table:
        if i != None and type(i) != int:
            raise DocumentDataError("The routing table must be made up of integers or None types.")


    if type(content_data) is list:
        tmp = ""
        for line in content_data:
            tmp += line + "\r\n"
        content_data = tmp

    frame_dict = {
        "pid": {"page-no": page_no, "frame-id": frame_id},
        "visible": True,
        "header-text": header_text,
        "cache-id": str(uuid.uuid4()),
        "frame-type": f_type,
        "content": {"data": content_data, "type": content_type},
        "title": {"data": title_data, "type": title_type},
        "routing-table": routing_table,
        "cursor": cursor,
        "connection": {"address": connection_address, "mode": connection_mode, "port": connection_port},
        "author-id": author_id,
        "response-fields": response_fields,
        "navmsg-forecolour": nav_msg_forecolour,
        "navmsg-backcolour": nav_msg_backcolour,
        "navmsg-highlight": nav_msg_highlight,
        "static-page":static_page,
    }

    return frame_dict


def create_default_routing_table(page_no):
    # Create default routing table (entries 0-9)
    routing_table = []
    for n in range(0, 10):
        routing_table.append(n + (page_no * 10))

    pn = page_no
    while pn > 999:
        pn = pn // 10
    routing_table.append(pn)

    return routing_table


def create_frame_field(label: str, horz_position: int, vert_position: int, required: bool, length: int = 9,
                       field_type: FieldType = FieldType.alphanumeric, auto_submit: bool = True, password: str = False):
    ftype = str(field_type).split(".")[1]

    # create a field
    return {
        "label": label,
        "vpos": vert_position,
        "hpos": horz_position,
        "required": required,
        "length": length,
        "type": ftype,
        "auto_submit": True,
        "value": "",
        "valid": False,
        "password": password
    }


def create_default_routing_table(page_no):
    # Create default routing table (entries 0-9)
    routing_table = []
    for n in range(0, 10):
        routing_table.append(n + (page_no * 10))

    pn = page_no
    while pn > 999:
        pn = pn // 10
    routing_table.append(pn)

    return routing_table


def centre_text(s: str):
    r = re.findall("\[[A-Z]\]", s)
    l = len(s) - (2 * len(r)) - s.count('\x1b') - s.count('\r') - s.count('\n')

    if 0 < l <= 40:
        s = ' ' * math.floor((40 - l) / 2) + s

    return s


def decode_markup(s: str):

    if s is not None and len(s) > 0:
        s = s.replace("[R]", k_controls["alpha_red"])
        s = s.replace("[G]", k_controls["alpha_green"])
        s = s.replace("[Y]", k_controls["alpha_yellow"])
        s = s.replace("[B]", k_controls["alpha_blue"])
        s = s.replace("[M]", k_controls["alpha_magenta"])
        s = s.replace("[C]", k_controls["alpha_cyan"])
        s = s.replace("[W]", k_controls["alpha_white"])
        s = s.replace("[F]", k_controls["flash"])
        s = s.replace("[S]", k_controls["steady"])
        s = s.replace("[N]", k_controls["normal_height"])
        s = s.replace("[D]", k_controls["double_height"])

    return s


def _create_frame(frame_type_name, page_id):
    frame = None

    try:
        frame_type = k_frame_type_names[frame_type_name]

        if frame_type is not None:
            frame = frame_type(page_id)
        return frame

    except KeyError as ex:
        return None
