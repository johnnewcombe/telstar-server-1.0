from telstar.frame_field import *
from telstar.log_utils import *

logger = getlogger(__name__)


class ResponseProcessor:

    def __init__(self, fields):

        if len(fields) == 0:
            raise Exception

        self._current_field_index = 0
        self.action_response = ResponseAction(fields, self._current_field_index)

    def process_char(self, char):

        # note that a field could also be a panel
        field = self.action_response.get_current_field()
        self.action_response.is_password = field["password"]
        self.action_response.char_valid = self._append_char(char)

        # check that this field is complete
        # input terminated by # unless autocomplete, then also terminated by reaching length
        if char == '\x5F' or (field["auto_submit"] and len(field["value"]) >= field["length"]):

            # check validity, if field is not valid, just return leaving field.valid property at False
            if (field["required"] and len(field["value"]) > 0) or not field["required"]:

                # mark the field as valid
                field["valid"] = True

                # valid field so mark as such
                if self._current_field_index < len(self.action_response.fields) - 1:

                    # increment current field id, note that the response.current_field-index property
                    # is not incremented here it is done in the server after processing
                    self._current_field_index += 1

                else:
                    # no more fields so response is complete
                    self.action_response.response_complete = True

        # backspace
        elif char == '\b':
            if len(field["value"]) > 0:
                field["value"] = field["value"][:-1]
                self.action_response.is_delete = True
            else:
                self.action_response.is_delete = False


        return self.action_response

    def _append_char(self, char):

        # field = action_response.fields[self._current_field_index]
        field = self.action_response.get_current_field()

        #  only valid chars be entered into the buffer.
        if len(field["value"]) >= field["length"]:
            result = False

        elif char == '\x5f':
            result = True

        elif field["type"] == "numeric" and self._isnumeric(char) or \
                field["type"] == "alpha" and self._isalpha(char) or \
                (field["type"] == "alphanumeric" and self._isalpha_numeric(char)):

            field["value"] += char

            result = True

        else:
            result = False

        return result

    def _isalpha_numeric(self, char: chr):
        return self._isnumeric(char) or self._isalpha(char)

    @staticmethod
    def _isnumeric(char: chr):
        return 0x30 <= ord(char) <= 0x39

    @staticmethod
    def _isalpha(char: chr):
        return (0x3a <= ord(char) <= 0x7f)  or (ord(char) == 0x20)


class ResponseAction():

    def __init__(self, fields, current_field_index):
        self.fields = fields
        self.response_complete = False
        self.current_field_index = current_field_index
        self.char_valid = False
        self.is_delete = False
        self.is_password = False

    def get_current_field(self):
        return self.fields[self.current_field_index]
