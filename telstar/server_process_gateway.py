from asyncio import CancelledError

from tornado import gen
from telstar.gateway import *
from telstar.response_processor import *
from telstar.event_args import *
from telstar.server_processor import *
from telstar.log_utils import *

logger = getlogger(__name__)

class ServerProcessGateway(ServerProcessor):

    def __init__(self, settings: dict):
        super().__init__(settings)

    async def process(self, char, stream, baud_rate, cancel_event, object_args):

        try:

            gateway = object_args[1]
            echo_char = gateway.send_request(char)
            if echo_char is not None:
                # note that when echoing we send None for the cancel_event and set baud rate to maximum
                await self.write_to_stream(stream, 0, [echo_char], None)

            while not cancel_event.is_set() and gateway.connected:

                response = gateway.receive_response()

                if response is not None and len(response) > 0:

                    # remove all nulls with a list comprehension
                    response = [elem for elem in response if elem != 0]
                    await self.write_to_stream(stream, baud_rate, [chr(x).encode('utf-8') for x in response], cancel_event)

                # just a little sleep to relieve the processor
                await gen.sleep(0.05)

            object_args[1] = gateway

        except CancelledError as ex:
            logger.info("Rendering cancelled.")
        except Exception as ex:
            logger.exception("Exception: {0}, {1}".format(type(ex), str(ex)))
