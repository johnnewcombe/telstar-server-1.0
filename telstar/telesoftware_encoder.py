from telstar.routing import *
from telstar.globals import *
from telstar.log_utils import *

logger = getlogger(__name__)

nav_msg = "\x5f for index or *pg num\x5F:{0}".format("\x1b\x47")
nav_msg_next = "\x5f for next or *pg num\x5F:{0}".format("\x1b\x47")

debug = 0


class TelesoftwareEncoder:
    BLOCK_START = b'\x7C\x41'  # 'A'
    BLOCK_END = b'\x7C\x5A'  # 'Z'
    BLOCK_G = b'\x7C\x47'  # G
    BLOCK_I = b'\x7C\x49'  # I

    def __init__(self, filename, starting_page_id, settings, frame_type, dal):

        if not Routing.is_valid_frame_id(starting_page_id):
            # TODO create custom exception
            raise Exception
        self.starting_frame_id = starting_page_id[-1]
        self.page_number = starting_page_id[:-1]

        file = open(filename, 'rb')
        self.data = file.read()
        self.encoded_data = b''
        self.blocks = []
        self.filename = filename.split('/')[-1]
        self.settings = settings
        self.frame_type = frame_type
        self.dal = dal

        self.control_sequence = 0

    # this encoding places all byte values within the range $20-$7F
    def encode(self):

        self.control_sequence = 0
        self.encoded_data = b''
        raw_byte_count = 0

        for byte in self.data:

            raw_byte_count += 1

            if byte == 0x0d:
                self.append_encoded_data(b'\x4c', True)  # EOL

            elif byte == 0x20:
                self.set_control_sequence(0)
                self.append_encoded_data(b'\x7d', False)

            elif byte == 0x7c:
                self.append_encoded_data(b'\x45', True)

            elif byte == 0x7d:
                self.append_encoded_data(b'\x7d', True)

            elif byte == 0x7e:
                self.set_control_sequence(0)
                self.append_encoded_data(bytes([byte]), False)

            elif byte < 0x20:
                self.set_control_sequence(1)
                byte += 0x40
                self.append_encoded_data(bytes([byte]), False)

            elif byte < 0x80:
                self.set_control_sequence(0)
                self.append_encoded_data(bytes([byte]), False)

            elif byte < 0xa0:
                self.set_control_sequence(2)
                byte -= 0x40
                self.append_encoded_data(bytes([byte]), False)

            elif byte < 0xc0:
                self.set_control_sequence(3)
                byte -= 0x60
                self.append_encoded_data(bytes([byte]), False)

            elif byte < 0xe0:
                self.set_control_sequence(4)
                byte -= 0x80
                self.append_encoded_data(bytes([byte]), False)

            else:
                self.set_control_sequence(5)
                byte -= 0xA0
                self.append_encoded_data(bytes([byte]), False)

        self.append_encoded_data(b'\x46', True)
        self._enblock()  # self.blocks now contains the blocks
        self._create_frames()

    # Returns true if the sequence was updated
    def set_control_sequence(self, sequence):

        if self.control_sequence != sequence:
            self.control_sequence = sequence
            self.append_encoded_data(bytes([sequence + 0x30]), True)

            return True

        else:
            return False

    def append_encoded_data(self, byte, precede_with_escape=False):

        if ord(byte) > 0x7f:
            raise Exception

        # add the escape if required
        if precede_with_escape:
            self.encoded_data += b'\x7c'
        self.encoded_data += byte

    # this method creates the correct number of blocks based on the data, this method is called as part
    # of the encode process and should not be called seperately
    def _enblock(self):

        # data should now have been encoded as a single file
        # spec not clear whether 859 includes all block/chksm bytes
        # in this implementation all data and block markers and chksum falls within this value.
        max_chars_per_block = 859 - 12  # allow for BLOCK_END (2) and checksum (3) and the start sequence (7)

        # start
        id = ord(self.starting_frame_id)
        id += 1  # leave room for the header frame
        frame_id = bytes([id])  # typically 'b' or 'c'

        block = TelesoftwareEncoder.BLOCK_START + TelesoftwareEncoder.BLOCK_G + frame_id + TelesoftwareEncoder.BLOCK_I
        chars_available = max_chars_per_block

        # data blocks
        # max length 859
        for char in self.encoded_data:

            # TODO: Tests need to be made in relation to a 7c sequence crossing a boundary, is it allowed?

            if chars_available == 0 or (chars_available == 1 and char == 0x7c):

                # close current block
                block += TelesoftwareEncoder.BLOCK_END
                block += TelesoftwareEncoder.calc_checksum(self, block)
                self.blocks.append(block)

                # start new block
                # TODO if frame id > 26 add '0' to the page id and set frame_id = 97 ('a')
                id += 1
                frame_id = bytes([id])

                block = TelesoftwareEncoder.BLOCK_START + TelesoftwareEncoder.BLOCK_G + frame_id + TelesoftwareEncoder.BLOCK_I
                chars_available = max_chars_per_block
                block += bytes([char])
                chars_available -= 1

            else:
                block += bytes([char])
                chars_available -= 1

        # complete the last block
        block += TelesoftwareEncoder.BLOCK_END
        block += TelesoftwareEncoder.calc_checksum(self, block)

        # if block is not empty, add it to the list
        if len(block) > 6:
            self.blocks.append(block)

        # insert the header block
        self.blocks.insert(0, self._create_header())

    def _create_header(self):

        block_count = len(self.blocks)

        # header block
        block = TelesoftwareEncoder.BLOCK_START
        block += TelesoftwareEncoder.BLOCK_G
        block += self.starting_frame_id.encode('utf-8')
        block += TelesoftwareEncoder.BLOCK_I
        block += self.filename.encode('utf-8') + b'\x7c\x4c'  # EOL
        block += format(block_count, '03d').encode('utf-8')
        block += TelesoftwareEncoder.BLOCK_END
        block += TelesoftwareEncoder.calc_checksum(self, block)

        return block

    def _create_frames(self):

        page_count = 0
        current_page_id = self.page_number + chr(ord(self.starting_frame_id))

        for block in self.blocks:

            # create frame
            pid = Frame.get_pageid_elements(current_page_id)
            page_no = pid["page_no"]
            frame_id = pid["frame_id"]

            logger.debug('[tsencoder] Creating frame {0}{1}'.format(page_no, frame_id))

            title = ""
            content = block.decode("utf-8")
            routing_table = frame_document.create_default_routing_table(page_no)

            frame_doc = frame_document.create_doc(page_no=page_no, frame_id=frame_id, frame_type=TelesoftwareFrame,
                                                  content_data=content, content_type="raw",
                                                  title_data=title, title_type="markup",
                                                  author_id="system-frame-generator", routing_table=routing_table,
                                                  static_page=True,
                                                  cursor=False, connection_address="", connection_port=0,
                                                  connection_mode="", response_fields=[])

            primary = self.settings["dbcollection"] == "primary"
            self.dal.insert_document(frame_doc, primary)

            current_page_id = self.get_follow_on_page_id(current_page_id)
            # page_count += 1

        next_page = Frame.get_pageid_elements(self.get_follow_on_page_id(current_page_id))
        result = self.dal.purge_documents(next_page["page_no"], next_page["frame_id"])

        # delete the remainder of the pages i.e. from the next frame to frame z.
        # the value 25 is used as we are incrementing before deleting and we are zero based

        # while page_count < 25:
        #     page_count += 1
        #     next_page_id =  self.page_number + chr(97 + page_count)
        #     self.delete_frame(next_page_id)

    # def delete_frame(self, frame_id):
    #     filename = '{0}{1}.page'.format(self.path_to_pages, str(frame_id))
    #     if os.path.isfile(filename):
    #         os.remove(filename)

    def get_follow_on_page_id(self, current_page_id):

        '''
        Gets the next frame id e.g. 2001a returns 2001b etc. Zero routing is implemented e.g 2001z returns 20010a
        :param current_page_id:
        :return: The next frame ID.
        '''

        # get the frame id i.e. the letter element
        frame_id_asc = ord(current_page_id[-1])
        page_id = current_page_id[:len(current_page_id) - 1]

        # update frame indicator and include zero page routing
        if frame_id_asc < 97:
            raise InvalidFrameIdError('The frame ID is invalid.')
        elif frame_id_asc < 122:
            frame_id_asc += 1
        else:
            frame_id_asc = 97
            page_id += '0'

        return page_id + chr(frame_id_asc)

    @staticmethod
    def calc_checksum(self, data):

        checksum = 0
        for byte in data[2:-2]:
            checksum ^= byte

        return format(checksum, '03d').encode('utf-8')

#    @staticmethod
#    def clean(path_to_software):
#        for filename in glob.glob(path_to_software + '*'):
#            os.remove(filename)
