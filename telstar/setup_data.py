from telstar.frame import *
from telstar.log_utils import *

logger = getlogger(__name__)

k_system_blobs = {
    "logo":
        [
            "http://edit.tf/#0:QIECBAgQIECBAgQIECBAgQAqiBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIEAKoBihpiBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBACqAYoaYEph6iBAgQIECBAgQIECBAgQIECBAgQIECBAgQAqgGKGmBKYeoFgg6SBAgQIECBAgQIECBAgQIECBAgQIECBAgQBpgSmHqBYIOkgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQB6gWCDpIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQA6SBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECA",
            0, 7, 0, 39
        ],
}

# These pages are system pages that are designed using edit.tf
k_system_pages = {
    "6a":
        [
            "https://edit.tf/#0:QIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBthQIECAbUizKdSDSDR8PTL3w-UFPLy7aceXmgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgKpEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIA1SLMp1INJBw5b-2nJl5oM-Hpl74fPNB03oN_TRl5IECBAgDc_PPpl281iDpoy88qDDyyoMmXph07MuRBiy7N_dcgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgDsg8OHMaBUCCpl6csOPRs38t-3L00Yd2UNw5b-2nJp3Z0CBAgQIECBAGQIK2n0g6eeGVBo67d_Xkgzct-1B00ZUCBAgQIAyBAgQIECBAg2b92fLz6IOXXdu07s6DtpyZd_TL4QIECBAgDIECBAgQIECDnl5dtOPKuQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIA7NBOsVIswMgracmXf0y-EGbfyQdNGULOsBqfDLj6cuu1cgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBA0QVIsWNBsBqmjKg37tmndlC9MuzL0y-Ogbnl5dtOPKuQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECAskSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJEiRIkSJECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECA",
            InformationFrame,
            [60, 61, 602, 603, 604, 65, 66, 67, 68, 69, 0]
        ],
    "9a":
        [
            "https://edit.tf/#0:QIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgc4UCAosWLFixYsWLFixYsWLFixYsWLFixYsWLFixYsWLFixYsWIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQMUCCLOjyZ0WLSkzo6CpFp1EFCDHioECBAgQIECBAgQIECBAyQIKkWnUQR6UGhIkw0FCDHioECBAgQIECBAgQIECBAgQIEDNAgp2adSLNQSZ0aegQIECBAgQIECBAgQIECBAgQIECBAgQNkCCPBqRa8GygpxaVaTDi00CBAgQIECBAgQIECBAgQIECBA3QIIc-dUgw6iCJFqQZMymgQIECBAgQIECBAgQIECBAgQIEDlAgpxadOTPnIJM6NPpTYNSTPnIECBAgQIECBAgQIECBAgQMECCZPjoJ8aMgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQICixYsWLFixYsWLFixYsWLFixYsWLFixYsWLFixYsWLFixYgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECAFUQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgBVAMUNMQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIAVQDFDTAlMPUQIECBAgQIECBAgQIECBAgQIECBAgQIECAFUAxQ0wJTD1A0EHSQIECBAgQIECBAgQIECBAgQIECBAgQIECANMCUw9QNBB0kCBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECAPUDQQdJAgQIECBAgQIECBAgQIECAosWLFixYsWLFixYsQIECAHSQIECAosWLFixYsWLFixYsWIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECA",
            InformationFrame,
            [90, 91, 92, 93, 94, 95, 6, 97, 98, 99, 0]
        ],
    "91a":
        [
            "https://edit.tf/#0:QIECBAgQIEEXdn07suXlp3Z0FTLz6IKGHPlQIECBAgQIECACAAgAIACAAgAIACAAgAIACAAgAIACAAgAIACAAgAIACAYMS54fzJmix4-YCDToOLOjyZ0WLSkzo6AkcGHuZUcRHlB4dgyAgAIACAAgAIACAAgAIACAAgAIACAAgAIACAAgAIACAAgGDP9__f_3_9__f_3_9__f_3_9__f_3_9__f_3_9__f_3_9_YNCho9zImSoAqBGoAp0FUy8-iChhz5UCA4MPEuZYwTAFzAFg1AgAIACAAgAIACAAgAIACAAgAIACAAgAIACAAgAIACAAgGDYCAAoACACeQHkBdYTJlixIkSKlSJEoUKIEBQABAAQAEABYN_3_9__f_3_9__f_3_9__f_3_9__f_3_9__f_3_9__f_39g4AgAIACAAgAIACAAgAIACAAgAIACAAgAIACAAgAIACAAgGDn9__f_3_9__f_3_9__f_3_9__f_3_9__f_3_9__f_3_9_YsAIACAAgAIACAAgAIACAAgAIACAAgAIACAAgAIACAAgAIBix_f_3_9__f_3_9__f_3_9__f_3_9__f_3_9__f_3_9__f2LICAAgAIACAAgAIACAAgAIACAAgAIACAAgAIACAAgAIACAYs_3_9__f_3_9__f_3_9__f_3_9__f_3_9__f_3_9__f_39i0AgAIACAAgAIACAAgAIACAAgAIACAAgAIACAAgAIACAAgGLX9__f_3_9__f_3_9__f_3_9__f_3_9__f_3_9__f_3_9_Ytq-jT0yg7OXZs39w0Pzh3Ao_LLl3BZuHPl3dMIGllyBIWzrlLmkKJGTSJUycsoUqlZJYtXLzLBiyZlWjVs3IuHLp2UePXz9AgQokaBIlTJ0ChSqVoFi1cvQMGLJmgaNWzdA4cunaB49fP0ECDChoIkWNHQSJMqWgmTZ09BQo0qaCpVrV0FizatoLl29fQYMOLGgyZc2dBo06taDZt3b0HDjy5oOnXt3QePPr2g-ff38pgw4sZHJlzZyujTq1ktm3dvNcOPLmW6de3cn48-vZf59_fwZiHv3Y8uHYIjbMPPQDVCxcLf4E0-mXDk8mI-_dlFCn5a9_QIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECA",
            TestFrame,
            [910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 9]
        ],
    "92a":
        [
            "http://edit.tf/#0:QIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQICn_OmTJkSJApUqVKlSpUlcHApiTJkyZEiRIkyZMmTIkSJFmKf3JX58____RBg-fPnzog__2iBBo-fNX9qgQcOHDh8-fGiAp_zFf______2v7_____7X___oEGr__Qa0ur_________QoCn9ywK__6D__a__7dH__tf___1QIv_9AiQIkaPX_bo0aMpiKf_-Yr__oP_9r__tUH_-1____9og__yjnz5cldX9qUc-fPkp__uSv_-g__2v_-1Qf_7X__aIv____KZv_dIV__2pTN___yn__mK___z__a____5__tf_9qgVf__8o5_vWJX__alHP___Kf_7kr____6dBr_____-h__2qBBr__ymbPnSFf69CUzf__8pnz5iq9ejRoECBWvXr0KBGjQYOHBHz4cECBAgwcCily7__yqDhw4ePnzogQeP7RAg0fPiBB___9X___QYPH___-sCmb__KoP________3B__tUCDV__tUH___1f36XB-_______KOf_8qgRo__9B__v9X___QINX___Qav__B8-NNX_-jR___8pm__ymYrq______h_odX9qg1f___7q__9X_-h1f_5TBgwYM37__KOSur-_Ro0Pr_8_f_qDV_b___r__1f_6D___lFKlSpUu__8pmK_v7VAgwf______tNX9r______V__oP__-gQaPnwpm__yqDB__tUCD__QIEG___1f2qL____9X_-g____z5-__ymb__KoNX_-lQKl6VAgQav6_V__oNf___1f2qBX_____9-hKOf_8qgVJ0JRw4cOPLkqiQoESNGgVL0aFEjQoECJGjRoymDNm__ymTJkzZv3_____-3bt27du3Thw8efPnz58-fPnz58-fP___QIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECA",
            TestFrame,
            [920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 9]
        ],
    "97a":
        [
            "https://edit.tf/#0:QIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBA5b4UCAosWLFixYsWLFixYsWLFixYsWLFixYsWLFixYsWLFixYsWIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgDVNGVBUizKdSDSQdtOTLv6ZfHRBz88-mXag080GFAgQIECANt35MvLcgw8OGzTjw9NO_cg6aMPRBw5b-2nJl5oMKBAgQIA3PTt4bMqDtpy98mHphX9tOTLv6ZfCDhsw9M2_ltQIECBAgDc9O3Tsw8kHTeg6aN_PKg6aMPRB3y8sqDhyy9suzLu6IECANk68tO7Og6aMqBi5bsOaDDuyIGLlww5rkCBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgDIECChsy4eeVBzy5Qejp04c3S9fn2YefPp08rse_agQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIA0HZ0y8t2Hpp7ZdnlBw2ZcPPKgzZcuxB03oM-Xog07kCBAgDdN_XHoQdtOEHq3Ze-PftxZeuuBn24dOxdj37UCBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIAVRAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECAFUAxQ0xAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgBVAMUNMCUw9RAgQIECBAgQIECBAgQIECBAgQIECBAgQIAVQDFDTAlMPUDQQdJAgQIECBAgQIECBAgQIECBAgQIECBAgQIA0wJTD1A0EHSQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIA9QNBB0kCBAgQIECBAgQIECBAgQICixYsWLFixYsWLFixAgQIAdJAgQICixYsWLFixYsWLFixYgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECA",
            InformationFrame,
            [910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 9]
        ],
}

k_redirect_pages = [
    {"pid": {"page-no": 0, "frame-id": "a"}, "redirect": {"page-no": 9, "frame-id": "a"},  "visible": True,}
]
