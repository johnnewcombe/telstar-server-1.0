from typing import List, Set, Dict, Tuple, Optional
from telstar.frame import *
from telstar.routing import *
from telstar.log_utils import *
from abc import ABC

logger = getlogger(__name__)


class EventArgs(ABC):
    pass


class ResponseCompleteEventArgs(EventArgs):

    def __init__(self, frame: Frame):

        self.frame = frame
        self.dynamic_frames = []


class UpdateEventArgs(EventArgs):
    pass
