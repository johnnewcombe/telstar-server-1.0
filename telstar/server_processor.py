from abc import ABC, abstractmethod
from typing import List, Set, Dict, Tuple, Optional
from tornado import gen
from telstar.globals import *
from telstar.log_utils import *

logger = getlogger(__name__)



class ServerProcessor(ABC):

    @abstractmethod
    def __init__(self, settings: dict):

        self.settings = settings

    @abstractmethod
    async def process(self, char, stream, baud_rate, cancel_event, object_args):
        pass

    async def _render_full(self, stream, baud_rate, render_buffer, cancel_event, frame):

        # populate any placeholders
        frame.content = frame.content.replace("[SERVER]", self.settings["server"])

        frame.render(render_buffer, self.settings["hide_pageid"], self.settings["hide_navigation_message"], self.settings["navigation_row"])
        await self.write_to_stream(stream, baud_rate, render_buffer, cancel_event)

    # Renders the frame with nav_message
    async def _render_partial(self, stream, baud_rate, render_buffer, cancel_event, nav_message, frame, restore_nav_message = True, nav_msg_sleep = 0.5):

        # TODO: expand this to include system message

        # by doing two partial renders and putting the original message back we get a nice effect at the client
        logger.info("{0} Partial Render: '{1}'".format(frame.page_id, nav_message))

        nav_msg = frame.nav_msg
        frame.nav_msg = nav_message
        frame.render_partial(render_buffer, self.settings["hide_navigation_message"], self.settings["navigation_row"])
        await self.write_to_stream(stream, baud_rate, render_buffer, cancel_event)

        if restore_nav_message and not self.settings["hide_navigation_message"]:
            await gen.sleep(nav_msg_sleep)
            frame.nav_msg = nav_msg
            frame.render_partial(render_buffer)

            await self.write_to_stream(stream, baud_rate, render_buffer, cancel_event)

    async def write_to_stream(self, stream, baud_rate, render_buffer, cancel_event):

        for b in render_buffer:
            if cancel_event is not None and cancel_event.is_set():
                logger.info('Abort streaming')
                break

            await stream.write(b)
            await gen.sleep(baud_rate)

    @staticmethod
    def string_to_buffer(btext):
        if type(btext) == str:
            btext = btext.encode('utf-8')
        return [chr(byt).encode('utf-8') for byt in btext]
