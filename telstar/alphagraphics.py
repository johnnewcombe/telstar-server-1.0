from telstar.frame import *
from telstar.data_import import *
from telstar.log_utils import *

logger = getlogger(__name__)


alphagraphics = {
    'a': [1, 1], 'b': [4, 1], 'c': [7, 1], 'd': [10, 1], 'e': [13, 1], 'f': [16, 1], 'g': [19, 1], 'h': [22, 1],
    'i': [25, 1], 'j': [28, 1], 'k': [31, 1], 'l': [34, 1], 'm': [37, 1], 'n': [1, 5], 'o': [4, 5], 'p': [7, 5],
    'q': [10, 5], 'r': [13, 5], 's': [16, 5], 't': [19, 5], 'u': [22, 5], 'v': [25, 5], 'w': [28, 5], 'x': [31, 5],
    'y': [34, 5], 'z': [37, 5],
    'A': [1, 9], 'B': [4, 9], 'C': [7, 9], 'D': [10, 9], 'E': [13, 9], 'F': [16, 9], 'G': [19, 9], 'H': [22, 9],
    'I': [25, 9], 'J': [28, 9], 'K': [31, 9], 'L': [34, 9], 'M': [37, 9], 'N': [1, 13], 'O': [4, 13], 'P': [7, 13],
    'Q': [10, 13], 'R': [13, 13], 'S': [16, 13], 'T': [19, 13], 'U': [22, 13], 'V': [25, 13], 'W': [28, 13],
    'X': [31, 13],
    'Y': [34, 13], 'Z': [37, 13],
    '1': [1, 17], '2': [4, 17], '3': [7, 17], '4': [10, 17], '5': [13, 17], '6': [16, 17], '7': [19, 17], '8': [22, 17],
    '9': [25, 17], '0': [28, 17],
    ' ': [1, 21], "'": [4, 21], '.': [7, 21], ',': [10, 21], ':': [13, 21], ';': [16, 21]
}

k_alphagraphics_page = "https://edit.tf/#0:QIECBAgQIIOzhowoI_LDw0acfNBG37uiBAgQIECBAgQIECAugQIGiBAgQIECBogQIHiBAgQIGiBAkQIECRA0QIGiBAgQIC_Pm1bo2rdGlbo2vfm1eoEDdG1bo2rVAgQNUHX0waoEDdu1LrViVasSrViVasSrViRKgQLVjVKgSpUCBA1QJUCVKgQJUqUugQIECBAgQIECBAgQIECBAjRoUCBAgQIEaFAgQIECBAgQIC6BAgQIECBAgQIECBAgQIEDRAgQIECBAgQIECBAgQIECBAgLt0bVujat0bVujat0CDvzYt0CBqgatUDVq1a6_yFqga8-bUulQJVqxK9WJVqxqlQIFixKtQIFqxKjTIVq1KuVpFqxqtWJC6BAgQIECFAgQIEKBAgQIECBAgQIECBAgQIECBAgRo0KBAgLvFjR5sQPNiB4sYPEiB4kQPFjRogaNECBA0QNNCBogQPHjQv14NXq5o1QIGqBq9SIHqRA1wMHqxq1QIEDVA9XNGqBA1atS6VAlWrEq1YlWrEK1IgSoEC1YlSoEqVAgWJUCVAlWpECVKlLoECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECAu8WNHixo8WNHixo82IHmxAseJGiBo0QNGjRo0QNGiBosWNC7VA1aoGvXg1aoGr1c0WrGiBqgaoGrVA1atWvv-xWvErxYlLpUCVasSpUCBauQpUCVasSoEqBasSo0yFatSpUCVAlQLViQugQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIC-xAgWLGixY0aIEDxYkaIECxY0eLGjxY0eLGiBAgQIECBAgL6kCB4sSrFjVroQLVjR6saIEDV6sarVjVqgaoECBAgQIECAuqQIFqxIsWJVu5IsWJVqxKgQJVqxKgQJVqxKgQIECBAgQIC6BAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgLoECBKgQIECBAgQMECBggQIECBAgQIECBAgQIECBAgQIECAugQIECBAgQIECBAwQIGCBAgQIECBAgQIECBAgQIECBAgQIC6BAgQIECRAgaIECBAgQoECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECA"


class AlphagraphicString:

    # TODO handle the + operator so that two AlphagraphicStrings can be concatenated
    def __init__(self, text, text_rows):
        """
        This class is typically created by a call to Alphagraphics.create_alphagraphic_string.
        :param text_rows:
        """
        self.text = text  # allows consumer to retrieve original text
        self.text_rows = text_rows

    def __len__(self):

        rows = self.text_rows
        return len(rows[0])

    def to_blob(self, graphic_colour='\x57', htab=0, vtab=0):

        # ensure we have a string as theme constants could be passed in which
        # are bytes like objects
        graphic_colour = str(graphic_colour)

        # if the escape has already been added such as when theme constants are
        # passed in
        if graphic_colour.startswith('\x1b'):
            graphic_colour = graphic_colour[1:]

        blob = ''
        ht = ''

        for v in range(vtab):
            blob += '\x0a'
        for h in range(htab):
            ht += '\x09'

        for row in self.text_rows:
            if graphic_colour is not None:
                blob += ht + '\x1b' + graphic_colour + row + '\r\n'
            else:
                blob += ht + row + '\r\n'

        return blob

    def get_centre_offset_tabs(self, max_char_width):
        """
        Returns the number of tabs required to centre the alphagraphic string within the maximum character width
        specified.
        :param max_char_width:
        :return: The number of tabs to centre the alphagraphic string.
        """
        return round((max_char_width - self.__len__()) / 2)


def dumpHex(text):
    for b in text:
        print("{0} ".format(hex(ord(b))))


class Alphagraphics:

    def __init__(self):
        pass

    def create_alphagraphic_string(self, text):

        return AlphagraphicString(text, self._get_text_rows(text))

    def _get_text_rows(self, text):

        try:

            text_data = self._get_text_data(text)

            # text_data now contains each of the characters as 12 integers representing the charater (4 rows of 3)
            # in 'base band' format i.e. the characters are in the range 0x00 0x3F. Before being transmitted, those
            # chars between 0x20 and 0x3F will need to have 0x40 added and those between 0x00 and 0x1F will need
            # to have 0x20 added.

            # check for shifting required
            bit_shift_required = False;
            for char_data in text_data:

                if bit_shift_required:
                    # shift all of the bits over to the right. Each char is described with three columns but has a max
                    # width of 2.5 columns, therefore all characters will have enough space for this as we have not
                    # yet proportionally spaced them.
                    self._shift_pixels_right(char_data)

                # if the last column of each row is a zero then we can remove it to proportionally space
                self._proportionally_space(char_data)

                # check to see if the char has a right hand blank pixel border, if not
                # set the flag to ensure that the next char gets shifted right by one pixel
                bit_shift_required = not self._has_right_border(char_data)

            # this will return a list of four strings, one for each row
            return self._prestel_encode(text_data)

        except Exception as ex:
            logger.exception(ex)

    @staticmethod
    def _prestel_encode(text_data):

        # result defined for four rows of text
        result = ['', '', '', '']

        for char_data in text_data:

            cols = len(char_data[0])

            for r in range(4):

                for c in range(cols):

                    char = char_data[r][c]

                    if 0x00 <= char <= 0x1f:
                        result[r] += str(chr(char + 0x20))
                    elif 0x20 <= char <= 0x3f:
                        result[r] += str(chr(char + 0x40))

        return result

    @staticmethod
    def _shift_pixels_right(char_data):
        # TODO: if char without border doesn't use row 3, total_cols-1 for decenders then we only need to bitshift any chars that
        #       use bits 0, 2 and 4 in rows 0, 1 and 2. e.g. a 'j' can follow an f without the need to bit shift
        cols = len(char_data[0])

        for row in range(4):

            # loop through the columns in reverse order
            for i in range(cols - 1, -1, -1):

                # if not the last column, then ensure the value in current column bits 1, 3 and 5
                # are placed in the following column bits 0, 2 and 4
                if i < cols - 1:
                    # save the val of bits 1, 3 and 5
                    tmp = char_data[row][i] & 0x2a

                    # shift left to make them bits 0, 2 and 4
                    tmp = tmp >> 1

                    # store them in bits 0, 2 and 4 of the following col
                    char_data[row][i + 1] = char_data[row][i + 1] | tmp

                # set to only the value of bits 0, 2 and 4
                char_data[row][i] = char_data[row][i] & 0x15

                # shift left to move the valuesto bits 1, 3 and 5
                char_data[row][i] = char_data[row][i] << 1

                if i == 0:
                    # all shifted so blank the left hand border (pixels 0, 2 and 4 of col 0) by including only cols 1, 3 and 5
                    char_data[row][i] = char_data[row][i] & 0x2a


    @staticmethod
    def _proportionally_space(char_data):
        # assume char is minimum width (always something in col 0)
        cols = 1

        # traverse all but the first column
        for col in range(1, 3):

            # assume we can remove this column in order to proportionally space
            blank_col = True

            # check that each row in this column is blank
            for row in range(4):
                if char_data[row][col] != 0:
                    # row ot blank so this column must stay
                    blank_col = False;  # can"t remove column
                    break

            if not blank_col:
                cols += 1

        # we have a blank column so reduce the length of each row
        for i in range(4):
            char_data[i] = char_data[i][:cols]

        # check for blank right hand pixel border. This is used as a space characters, if a border does not exist the following
        # character will need to be shifted over a pixel. Bits of the byte represent the pixels as follows
        #   -------
        #    0 | 1
        #   -------
        #    2 | 3
        #   -------
        #    4 | 5
        #   -------
        #
        # Therefore, to check for a right hand border (i.e. no pixels set) simply check bits 1, 3 and 5 for each row


    @staticmethod
    def _has_right_border(char_data):
        cols = len(char_data[0])
        tmp = 0
        for i in range(4):
            tmp += char_data[i][cols - 1] & 0x2a  # bits 1, 3 and 5

        # return true if border exists
        return tmp == 0


    @staticmethod
    def _last_column_is_empty(char_data):
        # if the last column of each row is a zero then we can remove it to proportionally space
        if char_data[0][2] + char_data[1][2] + char_data[2][2] + char_data[3][2] > 0:
            return False
        else:
            return True

    @staticmethod
    def _get_text_data(text):
        data_import = EditTfImport()
        data_import.data = k_alphagraphics_page

        base_band_text = []

        for char in text:

            #print("Processing : {0}".format(char))

            # get chars coordinates
            if char in alphagraphics:
                coords = alphagraphics[char]

                # get the character data
                base_band_char = data_import.import_data_char(coords[0], coords[1])
                base_band_text.append(base_band_char)

        return base_band_text


    #     ###############################################################################
    #     # Example
    #     ###############################################################################
    #     alphagraphis = Alphagraphics()
    #
    #     # get a formatted blob in yellow
    #     # blob = ag.get_blob("Hello World", '\x53')
    #
    #     # alternativelly get rows of text to manipulate as required
    #     # text_rows = ag.get_text_rows("Formula 1:")
    #     # for row in text_rows:
    #     #    blob += '\x1b\x57' + row + '\r\n'
    #
    #     # get the first blob, the guardian logo
    #     blob1 = Blob.load_blob(settings, 'guardian_logo.blob')
    #
    #     # the second blob comes from the alphagraphics string
    #     ag_string = alphagraphis.create_alphagraphic_string("Formula 1")
    #
    #     # Guardian has an available width of 28 chars to display the text
    #     # so centre the text with htabs e.g.
    #     htab = round((28 - len(ag_string)) / 2)
    #     if htab < 0:
    #         raise AlphagraphicsFormatError("Alphagraphics String is too Long")
    #     blob2 = ag_string.to_blob('\x57', htab, 0)
    #
    #
    #     blob = Blob.blob_concat(blob1, blob2)
    #     default_theme = Theme("Default", theme_file)
    #

