#!/usr/bin/env python3
#
# Copyright 2009 Facebook
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

# Typical Response Codes
# 200 – OK. The request was successful. The answer itself depends on the method used (GET, POST, etc.) and the API specification.
# 204 – No Content. The server successfully processed the request and did not return any content.
# 301 – Moved Permanently. The server responds that the requested page (endpoint) has been moved to another address and redirects to this address.
# 400 – Bad Request. The server cannot process the request because the client-side errors (incorrect request format).
# 401 – Unauthorized. Occurs when authentication was failed, due to incorrect credentials or even their absence.
# 403 – Forbidden. Access to the specified resource is denied.
# 404 – Not Found. The requested resource was not found on the server.
# 500 – Internal Server Error. Occurs when an unknown error has occurred on the server.

# To start with...
#     api --port 25232 --dev --cookie-secret b6c7a826-96d6-4f45-88c4-3cf27cc2c647


import functools
import urllib
from functools import wraps
from typing import Optional, Awaitable, Callable
from urllib.parse import urlencode

from telstar.dal import *
from telstar.auth import *
from telstar.frame_document import *
from telstar.log_utils import *

logger = getlogger(__name__)

import json
import argparse
import asyncio
import tornado.escape
import tornado.ioloop
import tornado.locks
import tornado.web
import os.path
import uuid
import datetime
import random
from telstar.cache import *
from telstar.config import *
from tornado.options import define, options, parse_command_line

# Definitions of special type checking related constructs.  Their definitions
# are not used, so their value does not matter.

overload = object()
Any = object()

define("port", default=8888, help="port to listen on", type=int)
define("debug", default=False, help="run in debug mode")

# global _cache object
auth_cache = Cache()

status_dict = {
        "version": {"major": 1, "minor": 0, "patch": 0, "msg": "This version number represents all versions prior to 2.0.0"},
}

def authenticated(
    method: Callable[..., Optional[Awaitable[None]]]
) -> Callable[..., Optional[Awaitable[None]]]:
    """ Decorate methods with this to require that the user be logged in.
        The decorator causes a check against the hendler.current_user, it
        there is no current user then the HTTP403 exception is raised.
        If it does exist then the the page id is checked against a user id
        if that is OK the function it wraps is executed.

        The handler.get_current_user returns a secret cookie which is only
        set once a successful login has been undertaken via the login url.
    """
    @functools.wraps(method)
    def wrapper(  # type: ignore
        self: tornado.web.RequestHandler, *args, **kwargs
    ) -> Optional[Awaitable[None]]:

        # if user cookie not set then check user against db and set cookie
        # if user cookie not set then check user against db and set cookie
        if not self.current_user:
            raise tornado.web.HTTPError(403)
        else:
            # logged on user but need to see page restrictions e.g. user 800 only has
            # access to pages that start with pages at 800( e.g. 8001, 80012 etc.)

            # get the token
            token = tornado.escape.xhtml_escape(self.current_user)

            # look up the user in the cache
            creds = auth_cache.get(token)
#            user_id = None if creds is None else int(tornado.escape.url_unescape(creds[0]))
            user_id = None if creds is None else creds[0]

            if user_id is None or (user_id != 0 and type(self) == UserHandler):
                raise tornado.web.HTTPError(403)

            # if user id is 0 the following checks are not needed
            # if > 0 then the UserHandler is out of bounds
            if user_id != 0:
                # getting the page number for each method is different
                if self.request.method in ("GET", "DELETE"):
                    # get the user_id
                    if user_id is None:
                        raise tornado.web.HTTPError(403)

                    if len(self.request.body) > 0:
                        get_data = tornado.escape.json_decode(self.request.body)
                        page_no = get_data["page_no"]
                    elif len(self.path_args) > 0:
                        get_data = self.path_args[0]
                        page_no = re.sub("[^0-9]", "", get_data)
                    else:
                        raise tornado.web.HTTPError(400)


                elif self.request.method in ("PUT"):
                    # get the user_id
                    if user_id is None:
                        raise tornado.web.HTTPError(403)

                    post_data = self.request.body
                    if post_data is None:
                        raise tornado.web.HTTPError(400)

                    pd = json.loads(post_data)
                    page_no = pd["pid"]["page-no"]

                    # fudge needed for file post requests
                    if len(args) > 0 and args[0] is None:
                        args = ()

                # check whether the user has access to the page
                if not (str(page_no).startswith(str(user_id)) or user_id == 0):
                    raise tornado.web.HTTPError(403)

        return method(self, *args, **kwargs)

    return wrapper


class BaseHandler(tornado.web.RequestHandler):

    # override this method to set current user
    def get_current_user(self):
        # e.g. get user from a cookie
        return self.get_secure_cookie("token", max_age_days=1)

    def write_error(self, status_code: int, **kwargs: Any):
        # invoke with raise e.g.
        #     raise tornado.web.HTTPError(400)
        self.set_status(status_code, self._reason)
        self.finish({"error": self._reason})

    @staticmethod
    def get_pageid_elements(page_id):

        result = {}

        if re.match(k_re_page_id, page_id) is not None:
            result = {"page_no": int(page_id[:-1]), "frame_id": page_id[-1:]}

        elif re.match(k_re_page_no, page_id) is not None:
            result = {"page_no": int(page_id)}

        return result

    def _validate_doc(self, json_doc: str):

        k_supported_frame_types = ["initial", "mainindex", "information", "exit", "gateway", "test", "response", "telesoftware", "system"]

        # k_frame_type_names = {"initial": InitialFrame, "mainindex": MainIndexFrame,
        #                       "information": InformationFrame, "exit": ExitFrame,
        #                       "gateway": GatewayFrame, "interactive": InteractiveFrame,
        #                       "test": TestFrame, "telesoftware": TelesoftwareFrame,
        #                       "response": ResponseFrame, "system": SystemFrame,
        #                       "exception": ExceptionFrame}



        # TODO: may need to expand this or move it to the dal
        # TODO only allow information dynamic_frames? or stop system dynamic_frames ?
        #  How would response dynamic_frames work or telesoftware?
        try:
            if json_doc is None:
                return False

            # FIXME this isn't the right way to check the size of the request
            if len(json_doc) > 2048:
                return False

            # check for a minimum frame
            if "pid" not in json_doc:
                return False
            if "page-no" not in json_doc["pid"] or "frame-id" not in json_doc["pid"]:
                return False

            # apply constraints
            if "frame-type" in json_doc and json_doc["frame-type"] not in k_supported_frame_types:
                return False

            #if edittf frame then content must be either
            if "content" in json_doc:
                if json_doc["content"]["type"] == "edit.tf":
                    data = json_doc["content"]["data"].split(":")[2]
                    if len(data) != 1120 and len(data) != 1167:
                        return False

            return True

        except Exception as ex:
            return False

class MainHandler(BaseHandler):

    def initialize(self, dal, auth):
        self.dal = dal
        self.auth = auth

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    def get(self):
        try:
            self.render("index.html")

        except Exception as ex:
            raise tornado.web.HTTPError(400)

class StatusHandler(BaseHandler):

    def initialize(self, dal, auth):
        self.dal = dal
        self.auth = auth

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    def get(self):
        try:

            self.write(json.dumps(status_dict))

        except Exception as ex:
            raise tornado.web.HTTPError(400)


class My404Handler(BaseHandler):

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    # Override prepare() instead of get() to cover all possible HTTP methods.
    def prepare(self):
        raise tornado.web.HTTPError(404)


    # Override prepare() instead of get() to cover all possible HTTP methods.
    def prepare(self):
        raise tornado.web.HTTPError(401)


class UserHandler(BaseHandler):
    def initialize(self, dal, auth):
        self.dal = dal
        self.auth = auth

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    @authenticated
    def get(self, user_id=None):

        try:
            user = self.auth.get_user(user_id)
            if user is not None:
                self.write(json.dumps(user))
            else:
                raise tornado.web.HTTPError(201)

            return self.auth.get_user(user_id)
        except:
            raise tornado.web.HTTPError(400)

    @authenticated
    def put(self):
        try:
            data = json.loads(self.request.body.decode("utf-8"))
            user_id = data["user_id"]
            password = data["password"]
            response = self.auth.insert_user(user_id, password)
            if response is not None:
                self.write('{"adduser": "ok"}\r\n')
            else:
                raise tornado.web.HTTPError(400)
        except Exception as ex:
            raise tornado.web.HTTPError(400)

    @authenticated
    def delete(self, user_id = None):
        try:
            # TODO Test this
            if len(self.request.body) > 0:
                data = tornado.escape.json_decode(self.request.body)
                user_id = data["user_id"]

            response = self.auth.delete_user(int(user_id))
            if response is not None:
                self.write('{"deleteuser": "ok"}\r\n')
            else:
                raise tornado.web.HTTPError(400)
        except:
            raise tornado.web.HTTPError(400)


class LoginHandler(BaseHandler):

    def initialize(self, dal, auth):
        self.dal = dal
        self.auth = auth

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    def put(self):

        try:
            data = tornado.escape.json_decode(self.request.body)

            un = data["un"]
            pw = data["pw"]

            authenticated = False
            self.clear_cookie("token")

            # don't want blank passwords but dont care if they
            # are weak, after all it is the 1980s, right?
            if len(pw) > 0:
                try:
                    # just in case a string was sent
                    user_id = int(un)

                    user = self.auth.get_user(user_id)
                    if user is not None:
                        stored_pw = user["hash"]
                        authenticated = self.auth.verify_password(stored_pw, pw)

                except ValueError as ex:
                    pass

            if authenticated:
                token = str(uuid.uuid4())
                auth_cache.update(token, [un, pw])
                self.set_secure_cookie("token", token, expires_days=1)
                self.write('{"login": "ok"}\r\n')

            else:
                raise tornado.web.HTTPError(401)
        except Exception as ex:
            raise tornado.web.HTTPError(400)

class PublishHandler(BaseHandler):

    def initialize(self, dal, auth):
        self.dal = dal
        self.auth = auth

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    @authenticated
    def get(self, frame_id):
        try:
            pid = self.get_pageid_elements(frame_id)

            if pid is not None:

                response = self.dal.publish_document(pid["page_no"], pid["frame_id"])

                if response is not None:
                    self.write('{"publish": "ok"}\r\n')
                else:
                    raise tornado.web.HTTPError(400)
            else:
                raise tornado.web.HTTPError(400)

        except tornado.web.HTTPError as ex:
            raise
        except Exception as ex:
            raise tornado.web.HTTPError(400)

    def put(self):
        pass


class FrameHandler(BaseHandler):

    def initialize(self, dal, auth):
        self.dal = dal
        self.auth = auth

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    @authenticated
    def get(self, page_id = None):

        try:
            pid = None

            primary =  "db" in self.request.arguments and self.request.arguments["db"][0] == b"primary"

            # supports page_id (i.e. page_no + frame_id) as a single param in json body
            # supports pip in json body
            # supports page_id (i.e. page_no + frame_id) as last path element of the url
            if len(self.request.body) > 0:
                data = tornado.escape.json_decode(self.request.body)
                if "page_id" in data:
                    pid = self.get_pageid_elements(data["page_id"])
                else:
                    pid = data
            else:
                try:
                    pid = self.get_pageid_elements(page_id)
                except:
                    pass

            if pid is not None:

                # Get document include invisible ones
                doc = self.dal.get_document(pid["page_no"], pid["frame_id"], None, True, primary)
            else:
                doc = self.dal.get_all_documents(primary)

            if doc is not None:
                # could be a mongo collection which is not serializable
                #if doc is type(mongo)
                self.write(json.dumps(doc))
            else:
                # no frame with that frame id so return appropriate response code
                raise tornado.web.HTTPError(404)

        except tornado.web.HTTPError as ex:
            raise
        except Exception as ex:
            raise tornado.web.HTTPError(400)

    @authenticated
    def put(self):

        try:
            response = None

            primary = "db" in self.request.arguments and self.request.arguments["db"][0] == b"primary"

            data = tornado.escape.json_decode(self.request.body)
            if self._validate_doc(data):
                response = self.dal.insert_document(data, primary)

            if response is not None:
                self.write('{"frame": "ok"}\r\n')
            else:
                raise tornado.web.HTTPError(406)
        except tornado.web.HTTPError as ex:
            raise
        except Exception as ex:
            raise tornado.web.HTTPError(400)

    @authenticated
    def delete(self, page_id = None):

        try:
            primary =  "db" in self.request.arguments and self.request.arguments["db"][0] == b"primary"

            # supports page_id (i.e. page_no + frame_id) as a single param in json body
            # supports pip in json body
            # supports page_id (i.e. page_no + frame_id) as last path element of the url
            if len(self.request.body) > 0:
                data = tornado.escape.json_decode(self.request.body)
                if "page_id" in data:
                    pid = self.get_pageid_elements(data["page_id"])
                else:
                    pid = data
            else:
                pid = self.get_pageid_elements(page_id)

            if pid is not None:

                response = self.dal.delete_frame(pid["page_no"], pid["frame_id"], primary)

                if response is not None:
                    self.write('{"delete": "ok"}\r\n')
                else:
                    # no frame with that frame id so return appropriate 4nn response code
                    raise tornado.web.HTTPError(400)
            else:
                raise tornado.web.HTTPError(400)

        except tornado.web.HTTPError as ex:
            raise
        except Exception as ex:
            raise tornado.web.HTTPError(400)


# TODO: Look at the tornado chat example to understand async operations
class WebServer:

    def __init__(self, port:int, settings: dict, url_prefix: str = "telstar"):

        self._port = port
        self.settings = settings

        if len(url_prefix) > 0:
            url_prefix = r"/" + url_prefix

        self.url_prefix = url_prefix

        dbcon = self.settings["dbcon"]
        if len(dbcon) == 0:
            raise DatabaseConnectionNotFoundError()

        self.dal = Dal(dbcon)
        self.auth = Auth(dbcon)

    def get_config_modified_time(self):
        return os.path.getmtime(self._config_file)

    def listen(self, cookie_secret: str, debug:bool = False):

        app = tornado.web.Application(
            [
                # Please note that for authentication to work only zero or one path element can exist following the
                # main 'handler' name e.g. /telstar/frame/101a is good /telstar/frame/101a/something is bad.
                (self.url_prefix, MainHandler, dict(dal = self.dal, auth = self.auth)),
                (self.url_prefix + r"/", MainHandler, dict(dal = self.dal, auth = self.auth)),
                (self.url_prefix + r"/status", StatusHandler, dict(dal=self.dal, auth=self.auth)),
                (self.url_prefix + r"/frame", FrameHandler, dict(dal = self.dal, auth = self.auth)),
                (self.url_prefix + r"/login", LoginHandler, dict(dal = self.dal, auth = self.auth)),
                (self.url_prefix + r"/frame/([^/]+)?", FrameHandler, dict(dal = self.dal, auth = self.auth)),
                (self.url_prefix + r"/publish/([^/]+)?", PublishHandler, dict(dal = self.dal, auth = self.auth)),
                (self.url_prefix + r"/user", UserHandler, dict(dal=self.dal, auth=self.auth)),
                (self.url_prefix + r"/user/([^/]+)?", UserHandler, dict(dal=self.dal, auth=self.auth)),
            ],
            cookie_secret=cookie_secret,
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            # TODO: check that this is safe, I am hoping that cross site scripting is
            #  negated because of the token used to authenticate every method.
            xsrf_cookies=False,
            debug=options.debug,
            default_handler_class=My404Handler
        )

        app.listen(self._port)
        tornado.ioloop.IOLoop.current().start()


