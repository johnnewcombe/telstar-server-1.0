import logging

k_logfile = "/var/log/telstar.log"
#k_logfile = "telstar.log"

def getlogger(name: str, level: int = logging.INFO):

    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(asctime)s : %(levelname)s : %(name)s : %(message)s')


    console_handler = logging.StreamHandler()
    console_handler.setLevel(level)
    console_handler.setFormatter(formatter)

    logger.handlers.clear()
    logger.addHandler(console_handler)

    # TODO Added as a quick and dirty way to create file based logs, wrapped like
    #  this as during Dev, permissions wont allow a write to /var/log
    try:
        file_handler = logging.FileHandler(k_logfile)
        file_handler.setLevel(level)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
    except:
        pass


    return logger