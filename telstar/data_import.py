from telstar.log_utils import *

logger = getlogger(__name__)


class DataImport:

    def __init__(self, filename = None):

        if filename is None:
            self.data = None
            #self.page_id = None
        else:
            self.data = self._get_data(filename)
            #self.page_id = filename.split('/')[-1].split('.')[0]

    def _get_data(self, path):
        """
        Get the url from the specified filename and removes any /r/n sequences that may exist at the end etc.
        The data is returned as a utf-8 string"""
        pfile = self.open_if_exists(path)
        if pfile is not None:
            data = pfile.read()
            pfile.close()

            if len(data) > 0:
                # convert from bytes object to string
                return data.decode('utf-8').replace('\r', '').replace('\n','').strip()
        else:
            raise FileNotFoundError("The data file was not found.")

    def import_data(self):
        raise Exception('Not Implemented')

    @staticmethod
    def open_if_exists(filename):
        try:
            fd = open(filename, "rb")

        except:
            return None
        else:
            return fd

    @staticmethod
    def get_page_id_from_filename(filename):

        #filename =''
        file = filename.split('/')[-1].split('.')[0]
        page_id = file.split('.')[0]
        return page_id

    @staticmethod
    def read_binary(filename):

        f = open(filename, "br")
        data = f.read()
        f.close()
        return bytearray(data)


# TODO: consider removing this as it is not currently used.
class BinaryImport(DataImport):

    def import_data(self):
        blob = self.data
        return blob


class EditTfImport(DataImport):

    def import_data(self, row_begin = 1, row_end = 22, column_begin = 0, column_end = 39, trim_ends = True):

        """
        Decodes the selected portion of edit.tf data into Prestel format, returns a string.
        :param row_begin:
        :param row_end:
        :param column_start:
        :param column_end:
        :return:
        """

        # col start must be < col end and row begin < row end etc.
        if row_end <= row_begin or column_end <= column_begin:
            raise IndexError

        # decode the url to get raw data
        raw_data = self._decode_url(self.data)
        cols_to_take = column_end - column_begin + 1

        # result goes here
        blob = ''

        # Teletext is 25 lines, Prestel/Telstar is 24, in addition line 0 is reserved for the Telstar header
        # and line 23 (24th line) is reserved for system messages, therefore
        # ignore first and last two lines of the raw data
        row_num = 0
        for row in range (row_begin, row_end + 1):

            row_num += 1

            # TODO: sort out row length... after the following strip

            # get the row but restrict to the row length specified
            row = raw_data[row*40:row*40 + 40][0:cols_to_take]

            # if this blob is to be contatenated then the call will probably
            # not want this trimmed
            if trim_ends == True:
                row = row.rstrip()

            rlen = len(row)

            for col in range(column_begin, min(cols_to_take + 1, rlen)):
                asc = ord(row[col])

                # for values 00 - 1F, add 40 and precede with an escape
                if 0x00 <= asc <= 0x1f:
                    asc += 0x40
                    blob += '\x1b'
                    blob += chr(asc)
                    #print('Viewdata Code: {0} {1}'.format(hex(0x1b), hex(asc)))
                else:
                    blob += chr(asc)
                    #print('Viewdata Code: {0} [{1}]'.format(hex(asc)[2:].zfill(2), chr(asc)))

            # as rstrip is used for each row, the row could be shorter than 40 chars
            if rlen < 40:
                blob += '\r\n'

        return blob

    def import_data_char(self, x, y):

        """
        Decodes a single character specified by xy from the edit.tf font pages, returns a list of 12 ints
        representing the graphic characters in 4 rows of 3 columns.
        Note that the graphics characters are in the range 0 to 3f.
        :param x:
        :param y:
        :return:
        """
        result = []

        # TODO Derive x/y from character map dict

        y1 = y + 4 # row start/end
        x1 = x + 3 # col start end

        raw_data = self._decode_url(self.data)

        row_num = 0
        for row in range(y, y1):

            rowresult = []
            row_num += 1
            row_data = raw_data[row * 40:row * 40 + 40]

            # TODO: Check for column end being beyond the row
            for col in range(x, x1):

                # this gives us the raw data byte but...
                i = ord(row_data[col])
                if i >= 0x60 and i <= 0x7f:
                    i -= 0x40
                elif i >= 0x20 and i <=0x3f:
                    i -= 0x20
                else:
                    i = 0

                rowresult.append(i)

            result.append(rowresult)

        return result

    # Decodes the url returning raw teletext data
    def _decode_url(self, encoded_url):

        alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";

        # If the URL contains a hash, remove everything up to and including it.
        hash_pos = encoded_url.find('#')
        encoded_data = encoded_url[hash_pos+3:]

        colon_pos = encoded_data.find(':')
        if(colon_pos > 0):
            encoded_data = encoded_data[:colon_pos]

        length = len(encoded_data)

        if length != 1120 and length != 1167 :
            logger.error("The encoded frame should be exactly 1120 or 1167 characters in length, it was {0}".format(length))
            # TODO Create custom exception
            raise Exception()

        # creates a populated list all 0's
        decoded_data = [0 for i in range(1000)]

        for index in range (0,len(encoded_data)):

            # this returns the index of the letter in alphabet that corresponds to the char in the url
            findex = alphabet.find(encoded_data[index])

            if findex == -1 :
                logger.error("The encoded character at position findex should be one from the alphabet")
                # TODO Create custom exception
                raise Exception()

            for b in range(0,6):

                # $val holds the index of the char

                bit = findex & ( 1 << ( 5 - b ))

                if bit > 0:
                    cbit = (index * 6) + b
                    cpos = cbit % 7
                    cloc = int((cbit-cpos) / 7)
                    decoded_data[cloc] |= 1 << (6 - cpos)

        # decoded data is a list of integers, so iterate over it and perform a join to get a string
        return ''.join([chr(n) for n in decoded_data])

