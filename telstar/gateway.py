import socket
from socket import *
from telstar.frame import *
from collections import deque
from tornado.iostream import StreamClosedError
from telstar.log_utils import *

logger = getlogger(__name__)


class Gateway:

    def __init__(self, show_cursor = True):

        self.socket_obj = socket(AF_INET, SOCK_STREAM)
        self.connected = False
        self.socket_obj.settimeout(5)
        self._quit_count = 0;
        self.buffer = deque([])
        self.connection = None

        self.overflow = False           # this condition is set if, after retreiving a page of formatted data, there is
                                        # still data existing in the buffer, i.e. a second page.

        self.show_cursor = show_cursor  # determines if a cursor is shown for the command line
        self._previous_char = None

        self._lines = deque([])
        self._words = deque([])

    def open(self, connection):

        try:
            self.connection = connection
            self.socket_obj.connect((connection["address"], connection["port"]))
            self.socket_obj.settimeout(0.01)  # reduce timeout as a crude but effective non-blocking approach
            self.connected = True

            logger.info('[gateway] Gateway opened.')

        except Exception as ex:
            logger.exception("[gateway] connection error ({0}:{1}) {2}".format(connection["address"], connection["port"], type(ex)))
            self.connected = False

    def send_request (self, char = b''):

        try:
            if len(char) > 0:

                if type(char) == str:
                    char = char.encode('utf-8')

                # This ensures that the 'any key' pressed to get here is not passed to the gateway
                # all subsequent ket presses are passed through as expected
                if self._previous_char is not None:

                    # check for three plus chars as this exits
                    if char == b'+':

                        # check if this makes three in total
                        if self._quit_count > 1:
                            # three received so close socket and return
                            self.close()
                            return
                        else:
                            self._quit_count += 1

                    # if we have an overflow it means that not all buffered data has been retrieved and the char received
                    # simply means send me the next page. In this case the char is used to reset the overflow and NOT sent
                    # to the gateway
                    if char == b'\x5F' and self.overflow:
                        self.overflow = False
                    else:
                        try:
                            self.socket_obj.send(char)
                        except timeout as ex:
                            pass

                        logger.info('[gateway] sent [{0}] to gateway.'.format(char))

                self._previous_char = char
                if self.connection["mode"] == "full_duplex_no_echo":
                    return char
                else:
                    return None

        except BrokenPipeError as ex:
            logger.info("[gateway] Disconnected: {0} Message: {1}".format(type(ex), str(ex)))
            self.close()
            return None
        except StreamClosedError as ex:
            logger.info("[gateway] {0} Disconnected: {1} Message: {2}".format(type(ex), str(ex)))
            self.close()
            return None
        except Exception as ex:
            logger.exception("[gateway] Send request error {0}".format(type(ex)))
            self.close()
            return None

    def receive_response(self):

        """
        This command will retreive any data from the socket and adding it to the buffer.
        :return:
        """
        data = b''
        byts = b''

        try:

            try:
                # Relies on short timeout to collect all data and
                # yet give a decent response time for the users key presses.
                byts = self.socket_obj.recv(1152)  # 1024 + 128 just to be safe
            except timeout as ex:
                pass

            if len(byts) > 0:
                data += byts
                # only log on dev machines as it is to onerous
                if self.connection["port"] > 10000:
                    for b in byts:
                         logger.info('[gateway] Bytes received from gateway: {0} [{1}]'.format(chr(b), hex(b)))

            if not (self.connection["mode"] == "viewdata" or len(
                    data) == 1 and data == self._previous_char):

                # add any lines to the list
                #for line in  data.decode().splitlines(True):
                #    self._lines.append(line.replace('\r\n', '\r').replace('\n', '\r\n').replace('\r', '\r\n'))
                for line in data.decode().splitlines():
                    self._lines.append(line)

                if not self.overflow:
                    data = self._get_page()

                if len(data) > 0 and self.show_cursor:
                    data = b'\x0c' + data + b':\x1b\x43'

            return data

        except BrokenPipeError as ex:
            logger.info("[gateway] Disconnected: {0} Message: {1}".format(type(ex), str(ex)))
            self.close()
            return None
        except StreamClosedError as ex:
            logger.info("[gateway] {0} Disconnected: {1} Message: {2}".format(type(ex), str(ex)))
            self.close()
            return None
        except Exception as ex:
            logger.exception("[gateway] send request error {0}".format(type(ex)))
            self.close()
            return None

    def buffer_empty(self):
        return len(self.buffer) == 0;

    def _get_page(self, line_count = 22):

        page = ''
        count = 0

        for i in range(line_count):
            count += 1

            if len(self._lines) > 0:
                if count < line_count:
                    page += self._lines.popleft() + '\r\n'
                else:
                    # this condition is set if, after retreiving a page of formatted data, there is
                    # still data existing in the buffer, i.e. a second page.
                    self.overflow = True
                    # exit the loop
                    break

        return page.encode('utf-8')


    def _add_words(self, data):

        # ensure we have a string
        if type(data) == bytes:
            text = data.decode()

        # get the lines
        lines = data.splitlines()

        # if the line is less than 39 chars, then ensure that there is a blank following it
        # this is a fudge to deal with lists and menus to ensure that they are displayed correctly.
        for line in lines:
            if len(line) < 39:
                for word in line.split():
                    self._words.append(word)
                    print("Word added:" + word)

                # add the blank, this will cause a cr to be rendered.
                self._words.append('')
                print("Word added: <blank>")
            else:
                for word in line.split():
                    self._words.append(word)
                    print("Word added:" + word)

    # This method is designed to format the content into a word wrapped block with each line less than 40 chars
    # It returns a list of paragraph lines.
    @classmethod
    def format(cls, text, max_line_length = 39):

        if type(text) == bytes:
            text = text.decode()

        formatted_text = []
        lines = 0
        text_line = ""

        # replace CRs
        text = text.replace('\r', ' ').replace('\n', ' ') # both exchanged for a space
        text = text.replace('  ', ' ')

        # get the words
        words = text.split(' ');

        # build each line of the formatted text
        for word in words:

            # add the space before counting the length
            word += " "

            if len(text_line) + len (word) <= max_line_length:
                text_line += word
            else:
                # add the line to the formatted text
                formatted_text.append(text_line.rstrip())

                # start a new line with the word that wouldn't fit
                if len(word) <= max_line_length:
                    text_line = word
                else:
                    text_line = "<Word too long>"

                lines += 1

        # add the final word and return
        formatted_text.append(text_line.rstrip())

        return formatted_text

    def close(self):
        try:
            self.socket_obj.close()
            self.connected = False
            logger.info('Goodbye!')

        except BrokenPipeError as ex:
            logger.info("[gateway] Disconnected: {0} Message: {1}".format(type(ex), str(ex)))
        except StreamClosedError as ex:
            logger.info("[gateway] {0} Disconnected: {1} Message: {2}".format(type(ex), str(ex)))
        except Exception as ex:
            logger.exception("[gateway] Error {0}".format(type(ex)))