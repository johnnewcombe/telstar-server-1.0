import pickle
import os
from telstar.exceptions import *
from telstar.blob import *
from telstar.log_utils import *

logger = getlogger(__name__)

k_blob_path = os.path.dirname(__file__) + "/data/"
# TODO: perhaps replace _get_blob(self, filename): in rss_page_generator

class Blob:

    @staticmethod
    def load_blob(path):

        if len(path) == 0:
            return None

        if not path.endswith('.blob'):
            path = path + '.blob'
        with open('{0}'.format(path), "rb") as file:
            blob = pickle.load(file)
        return blob

    @staticmethod
    def save_blob(path, blob):

        if len(path) == 0:
            return

        if not path.endswith('.blob'):
            name = path + '.blob'

        with open("{0}".format(path), "wb") as file:
            pickle.dump(blob, file)

    @staticmethod
    def blob_concat(blob1, blob2):

        '''
        Concatenates two blobs.
        :param blob1:
        :param blob2:
        :return:
        '''

        # tidy things up
        blob_rows1 = Blob._convert_to_rows(blob1)
        blob_rows2 = Blob._convert_to_rows(blob2)
        rcount_b1 = len(blob_rows1)
        rcount_b2 = len(blob_rows2)

        if rcount_b2 > rcount_b1:
            # concatenation not supported
            raise BlobConcatenationError("The second blob cannot have more rows than the first.")

        rows = min(rcount_b1, rcount_b2)

        # concatenate each row
        for i in range(rows):
            blob_rows1[i] += blob_rows2[i]

        result = ''

        # combine all rows
        for row in blob_rows1:
            # throw exception if row > 40 chars not including nonprintables
            count = 0
            for c in row:
                if c != '\x1b': # TODO: there are other non-printing that could be considered (cursor etc)
                    count += 1

            if count > 40:
                raise BlobConcatenationError("Concatenation creates rows > 40 charcters long.")
            result += row + '\r\n'

        return result

    @staticmethod
    def _convert_to_rows(blob):

        # determine any  \n (vtabs) at begining of blob, htabs are handled ok
        vtabs = 0
        for c in blob:
            if c == '\n':
                vtabs += 1
            else:
                break

        # remove the last element if it is blank, this is due to the final CR/LF being present on the blob
        blob = blob.rstrip('\r\n').lstrip('\n')

        # split blob into rows
        # not all rows will have CR/LF, some could be 40 chars long with no CR/LF
        # this could leave us with lines longer than 40 chars. This is not an issue as an
        # an exception is thrown if the concatenated blob is greater than 40 chars anyway.
        blob_rows = blob.split("\r\n")

        # precede with blank rows based on the number of vtabs
        for vt in range(vtabs):
            blob_rows.insert(0,"")

        return blob_rows
