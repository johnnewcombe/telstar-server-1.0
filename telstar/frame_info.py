from enum import Enum
from telstar.log_utils import *

logger = getlogger(__name__)


class FrameInfo:

    def __init__(self):
        # TODO: Change this to frame_id
        self.page_id = ''
        self.frame_state = FrameState.routing_message_updated
        self.error_message = ''
        self.next_page_id = ''
        self.last_frame_in_sequence = False
        self.frame = None


class FrameState(Enum):
    routing_message_updated = 0
    routing_message_deleted = 1
    page_found = 2
    invalid_page_request = 3
    page_not_found = 4


