from asyncio import CancelledError

from tornado import gen
from telstar.gateway import *
from telstar.response_processor import *
from telstar.event_args import *
from telstar.server_processor import *
from telstar.log_utils import *

logger = getlogger(__name__)


class ServerProcessRouting(ServerProcessor):

    def __init__(self, settings: dict):
        super().__init__(settings)

        self.response_complete_event = None

    async def process(self, char, stream, baud_rate, cancel_event, object_args):

        try:
            render_buffer = []

            # recover objects passed into this method
            frame = object_args[0]
            gateway = object_args[1]
            r_processor = object_args[2]
            routing = object_args[3]

            # check for updated settings
            # if self.config_modified_time < self.get_config_modified_time():
            #     self.settings = self.load_config(str(self.port))

            if frame is None:

                # use routing to get the frame id (probably the initial page as there is no previous page)
                # frame_info = routing.get_frame_id(char)
                # doesn't need to be _authenticated for this
                if routing.authenticated:
                    frame_info = routing.get_frame(self.settings["main_index_page"])
                elif routing.requires_authentication:
                    frame_info = routing.get_frame(self.settings["login_page"])
                else:
                    frame_info = routing.get_frame(self.settings["start_page"])

            else:

                # characters from response dynamic_frames and panel dynamic_frames are passed through a response processor
                if type(frame) ==  ResponseFrame:

                    # use a genetic frame_info until we know what frame we need to load etc.
                    frame_info = FrameInfo()

                    # process the incoming char in relation to the fields within the response frame
                    response = r_processor.process_char(char)
                    if char != '\x5F' and response.char_valid:
                        if response.is_password:
                            render_buffer.append('*'.encode('utf-8'))
                        else:
                            render_buffer.append(char.encode('utf-8'))
                        await self.write_to_stream(stream, baud_rate, render_buffer, cancel_event)

                    # backspace is a special case
                    elif response.is_delete:
                        render_buffer.append('\x08'.encode('utf-8'))
                        render_buffer.append('\x20'.encode('utf-8'))
                        render_buffer.append('\x08'.encode('utf-8'))
                        await self.write_to_stream(stream, baud_rate, render_buffer, cancel_event)


                    if response.response_complete:

                        # check for internal response pages
                        # Auth
                        if frame.page_no == self.settings["login_page"]:
                            logger.info('Login initiated for user {0}.'.format(frame.fields[0]["value"]))

                            # TODO: Check login details just set to True for now
                            routing.authenticated = True
                            # set self._authenticated
                            # load a specific frame using the routing process
                            #################################################
                            # TODO: move this to routing
                            #################################################
                            if routing.authenticated:
                                frame_info = routing.get_frame(0, frame)
                            else:
                                # TODO point this back at the initial page
                                self._logon_attempts += 1
                                if self._logon_attempts < 3:
                                    # initial page
                                    frame_info = routing.get_frame(99, frame)
                                else:
                                    # exit frame
                                    frame_info = routing.get_frame(90, frame)
                            #################################################

                        else:
                            await self._render_partial(stream, baud_rate, render_buffer, cancel_event, 'Processing...', frame, False)

                            args = ResponseCompleteEventArgs(frame)

                            if self.response_complete_event != None:
                                self.response_complete_event(args)

                            # manually create a frame_info object
                            frame_info = FrameInfo()

                            # make sure we have some pages to render
                            if len(args.dynamic_frames) > 0:
                                routing.temporary_frame_store = {}
                                base_frame_id = args.dynamic_frames[0].page_id
                                for result_frame in args.dynamic_frames:
                                    routing.temporary_frame_store[result_frame.page_id] = result_frame

                                # base_frame_id = next(iter(routing.temporary_frame_store))
                                frame_info.frame = routing.temporary_frame_store.get(base_frame_id)
                                frame_info.frame_state = FrameState.page_found
                                frame_info.page_id = frame_info.frame.page_id

                                # frame_info = args.frame_info
                                pass
                            else:
                                logger.info('There are no dynamic frames to display.'.format(frame.fields[0]["value"]))
                                frame_info.frame_state = FrameState.page_found
                                frame_info.frame = ExceptionFrame(frame.page_id)
                                routing.temporary_frame_store = {}


                    elif not response.response_complete and response.fields[response.current_field_index]["valid"]:
                        # field complete but response not complete so set focus to the next field
                        response.current_field_index += 1
                        frame.set_focus(render_buffer, response.current_field_index)
                        await self.write_to_stream(stream, baud_rate, render_buffer, cancel_event)

                else:
                    # use routing to get the frame id passing previous frame id
                    frame_info = routing.get_frame_id(char, frame)

            #logger.info('Message Buffer: {0}'.format(routing.message))

            if frame_info.frame_state == FrameState.page_found:

                # get the frame and set the settings attribute, this is used in cases
                # for conditional rendering and the testing of new features new features etc.
                frame = frame_info.frame

                # set this before the dynamic_frames are rendered
                object_args[0] = frame

                frame.settings = self.settings
                frame.parity_enabled = self.settings['parity']

                if len(frame.sys_msg) == 0:
                    frame.sys_msg = frame.settings['system_message']

                await self._render_full(stream, baud_rate, render_buffer, cancel_event, frame)
                render_buffer = [] # full render so empty buffer in case we render more for gateway or response

                logger.info('{0} Frame found and loaded.'.format(frame.page_id))

                if type(frame) == ExitFrame:
                    await stream.write('\x00\x00\x00'.encode('utf-8'))
                    stream.close()

                elif type(frame) == GatewayFrame:
                    # get the gateway destination
                    conn = frame.connection

                    # if gateway is not none then we have already visited the gateway and are now exiting
                    if conn is not None and gateway is None:
                        gateway = Gateway()
                        gateway.open(conn)
                        #await gen.sleep(0.5)
                        render_buffer = self.string_to_buffer('{0}    Press any key to continue...'.format('\x1e\n\n\n\n\n\n\n\n\n\n\x1b\x48'))

                    else:
                        gateway = None
                        render_buffer = self.string_to_buffer(
                            '{0}    Press 0 to return to TELSTAR...'.format('\x1e\n\n\n\n\n\n\n\n\n\n\x1b\x48'))

                    await self.write_to_stream(stream, baud_rate, render_buffer, cancel_event)

                elif type(frame) == ResponseFrame:

                    frame.set_focus(render_buffer)
                    await self.write_to_stream(stream, baud_rate, render_buffer, cancel_event)

                    r_processor = ResponseProcessor(frame.fields)

            elif frame_info.frame_state == FrameState.invalid_page_request:
                if frame is not None:
                    await self._render_partial(stream, baud_rate, render_buffer, cancel_event, "Invalid Page request.", frame)
                else:
                    logger.info("Expected page to be at '{0}'".format(self.settings['page_path']))

            elif frame_info.frame_state == FrameState.page_not_found:
                if frame is not None:
                    await self._render_partial(stream, baud_rate, render_buffer, cancel_event, "Frame not found.", frame)
                else:
                    logger.info("Expected page to be at '{0}'".format(self.settings['page_path']))

            elif frame_info.frame_state == FrameState.routing_message_deleted:

                if frame_info.frame_state == FrameState.routing_message_deleted:
                    render_buffer.append('\x08'.encode('utf-8'))
                    render_buffer.append('\x20'.encode('utf-8'))
                    render_buffer.append('\x08'.encode('utf-8'))
                    await self.write_to_stream(stream, baud_rate, render_buffer, cancel_event)

            elif frame_info.frame_state == FrameState.routing_message_updated:

                # echo the char to the user (backspace is handled separately
                if type(frame) != ResponseFrame and char != '\b':
                    render_buffer.append(char.encode('utf-8'))
                    await self.write_to_stream(stream, baud_rate, render_buffer, cancel_event)


            # write a final null to indicate to callers that this is the end of the stream writes.
            #await stream.write('\x00'.encode('utf-8'))

            # place frame in object store, can't use member variables as this class
            # supports multiple connections

            object_args[1] = gateway
            object_args[2] = r_processor
            object_args[3] = routing

        except StreamClosedError as ex:
            logger.info("{0} Message: {1}".format(type(ex), str(ex)))
        except CancelledError as ex:
            logger.info("Rendering cancelled.")
        except Exception as ex:
            logger.exception("Exception: {0}, {1}".format(type(ex), str(ex)))




