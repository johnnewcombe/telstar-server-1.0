from enum import Enum
from telstar.globals import *
from telstar.log_utils import *

logger = getlogger(__name__)

# state definitions for the telnet negotiation
class TelnetState(Enum):
    undefined = 0
    IAC_found = 1
    SB_found = 2
    DO_found = 3
    WILL_found = 4
    DONT_found = 5
    WONT_found = 6


class TelnetParser:

    """
    If you send an IAC WILL TRANSMIT-BINARY, you are talking about the direction from you to the peer and you are
    effectively asking the peer to allow you send binary. The peer can answer either  DO or DONT.

    If you send an IAC DO TRANSMIT-BINARY, you are instead talking about the direction from the peer to you and you
    are effectively asking the peer to start transmitting binary if it is able to and willing. The peer can answer
    either WILL or WONT.

    However, if the peer sends an IAC WILL TRANSMIT-BINARY (as opposed to you sending it), it is talking about the
    direction from the peer to you and it is effectively asking you to allow it send binary. You can answer
    either DO or DONT.

    Analogously, if the peer sends an IAC DO TRANSMIT-BINARY, it is talking about the direction from you to the
    peer and it is effectively asking you to start transmitting binary if you are able to and willing. You can answer
    either WILL or WONT.
    """

    def __init__(self, stream):

        self.telnet_state = TelnetState.undefined
        self._last_char_received = ''
        self.telnet_connection = False
        self.suppress_goahead = False
        self._stream = stream

    def parse(self, char):
        # The telnet negotiation starts with IAC (FF) followed by 2 bytes unless the 2nd byte is SB (FA) Sub-Negotiation
        # in which case all subsequent chars can be ignored until the sequence SE (FF FO) Sub-Negotiation End
        # Mute echo until all of the negotiation is done

        # State machine to sort out telnet parsing
        if self.telnet_state == TelnetState.undefined and ord(char) == 0xff:  # IAC
            logger.info('IAC Start')
            self.telnet_state = TelnetState.IAC_found
            char = b''

        elif self.telnet_state == TelnetState.IAC_found and ord(char) == 0xff and ord(self._last_char_received) == 0xff:
            logger.info('IAC Cancelled for FF FF')
            self.telnet_state = TelnetState.undefined
            # allow the char to continue

        elif self.telnet_state == TelnetState.IAC_found and ord(char) == 0xfa:  # SB. sub negotiation start
            logger.info('SB Started')
            self.telnet_state = TelnetState.SB_found
            char = b''

        elif self.telnet_state == TelnetState.SB_found and char == b'\xf0':  # SE, end of Sub-Negotiation
            # end of the command (sub negotiation)
            logger.info('SB Ended')
            self.telnet_state = TelnetState.undefined
            char = b'\x5f'

        elif self.telnet_state == TelnetState.IAC_found:

            if 240 <= ord(char) <= 255:
                logger.info('Telnet client detected.')
                self.telnet_connection = True

            # These will all be followed by options
            if ord(char) == 251:  # WILL
                self.telnet_state = TelnetState.WILL_found
                char = b''
            elif ord(char) == 252:  # WONT
                self.telnet_state = TelnetState.WONT_found
                char = b''
            elif ord(char) == 253:  # DO
                self.telnet_state = TelnetState.DO_found
                char = b''
            elif ord(char) == 254:  # DONT
                self.telnet_state = TelnetState.DONT_found
                char = b''
            elif 240 <= ord(char) <= 250:  # ignore these
                # no options for these so telnet complete
                logger.info('{0} command ignored IAC End'.format(hex(ord(char))))
                self.telnet_state = TelnetState.undefined
                char = b'\x5f'

        elif self.telnet_state == TelnetState.WILL_found:
            # anything arriving here is an option
            logger.info('WILL {0}'.format(hex(ord(char))))
            self.telnet_state = TelnetState.undefined
            char = b'\x5f'

        elif self.telnet_state == TelnetState.WONT_found:
            # anything arriving here is an option
            logger.info('WONT {0}'.format(hex(ord(char))))
            self.telnet_state = TelnetState.undefined
            char = b'\x5f'

        elif self.telnet_state == TelnetState.DO_found:
            # anything arriving here is an option
            logger.info('DO {0}'.format(hex(ord(char))))
            if ord(char) == 0x01:  # ECHO
                # return WILL echo in response to receiving the DO
                self._stream.write(b'\xff\xfb\x01')
            elif ord(char) == 0x03:  # GOAHEAD
                # return WILL suppress go ahead in response to receiving the DO
                # also indicate that I am will be echoing by sending WILL echo
                self._stream.write(b'\xff\xfb\x03')
            else:
                # send WONT to all other do requests
                self._stream.write(b'\xff\xfc' + char)

            logger.info('IAC End')
            self.telnet_state = TelnetState.undefined
            char = b'\x5f'

        elif self.telnet_state == TelnetState.DONT_found:
            # anything arriving here is an option
            logger.info('DONT {0}'.format(hex(ord(char))))
            self.telnet_state = TelnetState.undefined
            char = b'\x5f'

        # Telnet clients will either send CR/LF or CR/NULL the NULL can cause issues when detecting EOL
        # as it tends to around at the start of subsequent input strings
        char = char.replace(b'\x00', b'')
        char = char.replace(b'\n', b'')
        char = char.replace(b'\r', b'\x5f')

        self._last_char_received = char

        return char
