from telstar.frame import *
from telstar.frame_info import *
from telstar.content_dictionary import *
from telstar import frame_document
from telstar.log_utils import *

logger = getlogger(__name__)


# F5 = Telesoftware
# F7 = Esc
# F8 = Re-Display    '*00', '*09'
# F9 = Previous Page '*#'

class Routing:

    FRAME_NUMBER_REGEX = re.compile('^[0-9]{1,9}[a-z]{1}$')  # reg ex of 1-9 digits with one trailing letter
    # PAGE_NUMBER_REGEX = re.compile('^[0-9]{1,9}[a-z]{0,1}$')  # reg ex of 1-9 digits with one optional trailing letter
    PAGE_NUMBER_REGEX = re.compile('^[0-9]{1,9}$')  # reg ex of 1-9 digits with one optional trailing letter

    def __init__(self, settings, dal):

        self.main_index_page = settings["main_index_page"]

        self.dal = dal
        self.settings = settings
        self.message = ''
        self.last_good_message = ''
        self.immediate_mode = True
        self.history = []
        self.last_good_message = ''
        self.current_frame = None
        self.authenticated = False
        self.requires_authentication = settings["requires_authentication"]

        # # page mapping
        # f = open(path_to_page_map_file, 'rb')
        # self.page_map = pickle.load(f)
        # f.close()

        # temporary page store
        # this is used by services. The scenario is as follows.
        # Service page performs action e.g. page 250a performs search and presents page 251a,
        # further results could be created on dynamic_frames 251b, c, d, e... etc. these would be stored in the
        # temporary_frame_store and could be recovered through the normal routing process
        # NOTE: this is not the same as the page map as that maps one page id to another.
        #       the temporary page store stores the actual frame object in memory rather than on disk.
        self.temporary_frame_store = {}

    def get_frame(self, page_no: int, current_frame: Frame = None):

        """
        This method will return a populated frame_info object for the specified
        frame id. the method processes the frame through routing thereby updating
        the history and so on.
        """
        # fudge to get frame from the routing process
        # turn off immediate mode
        im = self.immediate_mode
        self.immediate_mode = False

        # route the page
        self.message = str(page_no)
        frame_info =  self.get_frame_id("_", current_frame)

        # reset immediate mode
        self.immediate_mode = im

        # return the page
        return frame_info

    def get_frame_id(self, char: chr, current_frame: Frame = None):

        self.current_frame = current_frame

        # Used later to fix up the message buffer
        history_page = False
        mapped_page = False

        # initialise the return object
        frame_info = FrameInfo()

        # validate char and message buffer etc
        # if the char is None exit early
        if char is None:
            frame_info.frame_state = FrameState.routing_message_updated
            logger.info('Char is None, routing complete.')
            return frame_info

        # check for start of non-immediate sequence
        elif char == '*':
            self.immediate_mode = False
            self.message = ''

        # backspace
        if char == '\b':
            if len(self.message) > 0:
                self.message = self.message[:-1]
                frame_info.frame_state = FrameState.routing_message_deleted
            return frame_info

        # the hash char doesn't go in the message
        else:
            self.message += char

        # check for large data request e.g. http etc. and truncate
        if len(self.message) > 11:
            frame_info.error_message = 'Message is too long, truncating.'
            # truncate the message
            if self.immediate_mode:
                self.message = ''
            else:
                self.message = '*'

        # Start of routing process
        logger.info('Routing Start. [Current Page: {0}] [Message Buffer: {1}] [Character to Process: {2}]'.
                         format("None" if self.current_frame == None  else self.current_frame.page_id, self.message, char))

        # reload the page
        # elif self.message == "*00" or self.message == "*09":  # Prestel special sequences
        # TODO: Fix this... These sequences don't seem to work.
        if self.message == "*00" or self.message == "*09":  # Prestel special sequences
        # at this point immediate mode will have been set to false due to the
            # * in the sequence so turn it back on
            self.immediate_mode = True

            # re-display (00) / update page (09)
            frame_info.page_id = self.frame.page_id

            logger.info('Page Id obtained from current page. [Page Id: {0}]'.format(frame_info.page_id))

        elif self.message == "*\x5f":
            # at this point immediate mode will have been set to false due to the
            # * in the sequence so turn it back on
            self.immediate_mode = True

            frame_info.page_id = self.get_previous_frame_id_from_history()

            # Used later to fix up the message buffer
            history_page = True

            logger.info('Page Id obtained from history: [Page Id: {0}]'.format(frame_info.page_id))

        elif self.immediate_mode and char == "\x5f":

            frame_info.page_id = self.get_hash_frame_id(self.current_frame.page_id)
            #frame_info.page_id = str(self.current_frame.routing_table[10] + self.current_frame.page_no)
            # remove any * or # chars
            self.message = self.message_trim(self.message)

        elif self.immediate_mode:

            if ord(char) >= 0x30 and ord(char) <= 0x39:
                #frame_info.page_id = str(self.current_frame.routing_table[ord(char) - 0x30] + self.current_frame.page_no)
                frame_info.page_id = self.current_frame.get_route_entry(ord(char) - 0x30)
            else:
                frame_info.frame_state = FrameState.page_not_found

        else:  # non-immediate mode

            if char == '\x5F':

                # remove any typed '*' and trailing '#'
                self.message = self.message_trim(self.message)

                if self.is_valid_page_number(self.message):
                    frame_info.page_id = self.message

                    logger.info('[non-immediate mode] Frame is valid. [Buffer: {0}]'.format(self.message))

                else:
                    frame_info.frame_state = FrameState.invalid_page_request
                    logger.info('[non-immediate mode] Frame is invalid. [Buffer: {0}]'.format(self.message))

                # once complete turn immediate mode back on
                self.immediate_mode = True
                self.message = ""

            else:  # non immediate mode but waiting for the \x5f terminator
                frame_info.frame_state = FrameState.routing_message_updated

        # routing complete
        logger.info('Routing finished [Message Buffer: {0}]'.format(self.message))

        # page id determined so check that the page exists and update history etc.
        if frame_info.page_id is not None and len(frame_info.page_id) > 0:

            # add the frame id if it is not present (belts and braces?)
            frame_info.page_id = self.fix_page_id(frame_info.page_id)

            # check map file in case we have an entry for this page
            # if frame_info.page_id in self.page_map:
            #     frame_info.page_id = self.page_map.get(frame_info.page_id)
            #     mapped_page = True
            #
            #     logger.info('{0} Frame is in Page Map. [Message Buffer: {1}]'.format(frame_info.page_id, self.message))

            # don't use the page exist() as this doubles the number of database requests
            frame_info.frame = self.get_static_frame(frame_info.page_id)

            if frame_info.frame is not None:

                self.current_frame = frame_info.frame

                # page exists so signal and update history
                frame_info.frame_state = FrameState.page_found

                self.add_to_history(frame_info.page_id)
                self.last_good_message = self.message
                self.message = ""

                # get the follow on page_id to aid with rendering etc. note that we are not using get_hash_frame_id
                # as we want to check for None.
                frame_info.next_page_id = self.get_follow_on_frame_id(frame_info.page_id)

                if frame_info.next_page_id is not None:
                    frame_info.frame.has_follow_on_frame = True

                logger.info('{0} Frame exists. [Message Buffer: {1}]'.format(frame_info.page_id, self.message))

            else:
                frame_info.frame_state = FrameState.page_not_found
                logger.info('{0} Frame not found. [Message Buffer: {1}]'.format(frame_info.page_id, self.message))
                self.message = ""

        # Tidy up the message
        # can't use frame_info.frame.page_no as frame could be None
        if len(frame_info.page_id) > 0 and frame_info.page_id == self.main_index_page:
            # clear the message as this is the index page from where navigation starts
            self.message = ''

        elif (frame_info.frame_state == FrameState.invalid_page_request or
              frame_info.frame_state == FrameState.page_not_found):
            self.message = self.get_last_good_message()
            logger.info('Resetting buffer (Frame not found/invalid etc.). [Message Buffer: {0}]'.format(self.message))

        # places the correct value in the message
        #   e.g. converts *# to the correct pageid in the case of the history page
        #   adds the redirected page to the message rather than the requested page
        elif mapped_page or history_page:
            # update the message text
            self.message = frame_info.page_id[:-1]  # dont need the frame indicator

        return frame_info

    def get_static_frame(self, page_id):

        # Check self.temporary_frame_store first
        frame = self.temporary_frame_store.get(page_id)

        if frame is None:

            # routing good so get the static frame unpickle and render
            pid = Frame.get_pageid_elements(page_id)
            primary_db =  self.settings["dbcollection"] == "primary"

            frame_doc = self.dal.get_document(page_no=pid["page_no"], frame_id=pid["frame_id"], primary_db=primary_db)

            # check if this is a redirect and get the redirected page

            if frame_doc is not None and "redirect" in frame_doc:

                redirect = frame_doc["redirect"]

                #Need to check that redirect is valid
                if len(redirect["frame-id"]) > 0:
                    frame_doc = self.dal.get_document(page_no=redirect["page-no"], frame_id=redirect["frame-id"],
                                                  primary_db=primary_db)
                    if "redirect" in frame_doc:
                        raise RedirectError("Only one page re-direct allowed.")

            frame =  frame_document.doc_to_frame(frame_doc)

        return frame

    def fix_page_id(self, page_id):

        if not self.FRAME_NUMBER_REGEX.match(page_id):
            page_id += "a"  # add the sub page letter

        # ensure that no page is ever preceeded with a zero except page 0a itself of course
        if page_id != "0a":
            page_id = page_id.lstrip("0")

        return page_id

    def page_exists(self, page_id):

        result = self.temporary_frame_store.get(page_id)

        if result is not None:
            return True

        return self.get_static_frame(page_id) is not None

    def get_follow_on_frame_id(self, current_id):

        """
        This function increments the frame index of the current_id and increments it.
        For example, if the current page is 200a, then 200b would be returned IF the page
        exists. If the page does not exist then the function returns None.
        :param current_id:
        :return: The incremented page id only if the new page exists, otherwise None.
        """

        page_id = None

        follow_on_id = ContentDictionary.get_follow_on_page_id(current_id)
        logger.info("Determining follow-on frame Id [Current frame Id {0}] [Follow on Frame: {1}]".format(current_id, follow_on_id))

        # if the page doesn't exist' return None
        if self.page_exists(follow_on_id):
            page_id = follow_on_id
            logger.info("Follow-On frame set to {0}".format(page_id))
        else:
            logger.info("No Follow-On frame.".format(page_id))

        return page_id

    def get_last_good_message(self):

        if len(self.history) > 0:
            return self.history[-1][:-1]
        else:
            return None

    def add_to_history(self, page_id):

        # mustn't add the same page twice
        if len(self.history) > 1 and self.history[-1] == page_id:
                return
        self.history.append(page_id)

    def get_previous_frame_id_from_history(self):

        if len(self.history) > 1:
            self.history.pop()  # remove current frame
            return self.history.pop()  # remove previous frame
        else:
            return self.get_current_frame_id_from_history()

    def get_hash_frame_id(self, current_id):

        '''
        This function returns the frame that should be returned if the hash '#' (\x5f) key is pressed.
        For example, if the current page is 200a, then 200b would be returned assuming the page
        exists. If the page does not exist then the function will use the page specified in routing_table[10].
        :param current_id:
        :return: The next page id to be displayed based on the rules described above.
        '''

        page_id = self.get_follow_on_frame_id(current_id)

        if page_id is None:
            page_id = self.current_frame.get_route_entry(10) + "a"
            logger.info("'Hash' Page Id obtained from routing table. [Page Id: {0}]".format(page_id))
        else:
            logger.info("'Hash' Page Id obtained from 'follow on' frame. [Page Id: {0}]".format(page_id))

        return page_id

    @staticmethod
    def message_trim(page_id):
        return page_id.lstrip("*").rstrip("\x5F")

    @staticmethod
    def is_valid_page_number(page_id):

        # check for a valid page id so far
        if Routing.PAGE_NUMBER_REGEX.match(page_id):
            return True
        else:
            return False

    @staticmethod
    def is_valid_frame_id(page_id):

        # check for a valid page id so far
        if Routing.FRAME_NUMBER_REGEX.match(page_id):
            return True
        else:
            return False

    @staticmethod
    def open_if_exists(filename):
        try:
            fd = open(filename, "rb")

        except:
            return None
        else:
            return fd
