import os
import pymongo
import filecmp
import shutil
import tempfile
from pathlib import Path
from pymongo import MongoClient
from telstar.frame_document import *
from telstar.log_utils import *

logger = getlogger(__name__)

# Public Key CJXLVRTP
# Private Key dafc7580-a09a-4c76-863e-9afb3ed113a1
# Organisation ID 5e8b0edfdf26162d6eb502b0
# Project ID 5e8b0edf906a771e908e4e0e

class Dal:

    def __init__(self, url: str):

        # TODO: consider passing in the client object
        client = MongoClient(url)
        self.db = client.telstardb

    def get_document(self, page_no: int, frame_id: chr, save_directory: str = None, visible_only: bool = True,
                     primary_db=False):

        logger.debug("Get document.")
        collection = self.get_collection_name(page_no, primary_db)

        if visible_only:
            filter = {"pid.page-no": page_no, "pid.frame-id": frame_id, "visible": True}
        else:
            filter = {"pid.page-no": page_no, "pid.frame-id": frame_id}

        frame_doc = self.db[collection].find_one(filter)

        if frame_doc is not None:

            self._remove_id(frame_doc)

            if save_directory is not None:

                while save_directory.endswith("/"):
                    save_directory = save_directory[:-1]

                json_frame = json.dumps(frame_doc)

                with open("{0}/{1}{2}.json".format(save_directory, page_no, frame_id), "w") as file:
                    file.write(str(json_frame))

        return frame_doc

    def get_all_documents(self, primary_db=False):

        logger.debug("Get all documents.")
        result = []

        # this returns a tuple with primary first followed by secondary
        c = self._get_collection_names()
        if primary_db:
            collections = c[0]
        else:
            collections = c[1]

        #print(collections)
        for collection in collections:
            #print(collection)
#            print(self.db[collection].find())
            # result would normally be a mongodb cursor, so convert to a dict
            frame_docs =  self.db[collection].find()

            # unfortunately all docoments need to have the object Id removed
            for frame_doc in frame_docs:
                self._remove_id(frame_doc)
                result.append(frame_doc)

        return result

    def get_documents_by_collection(self, collection):

        logger.debug("Get documents by collection.")
        return self.db[collection].find()

    def get_all_static_documents(self, collection):
        filter = {"static-page": True}
        return self.db[collection].find(filter)

    def get_all_non_static_documents(self, collection):
        filter = {"static-page": {"$ne": True}}
        return self.db[collection].find(filter)

    def insert_document(self, document: dict, primary_db=False):

        logger.debug("Insert document.")
        page_no = document["pid"]["page-no"]
        frame_id = document["pid"]["frame-id"]

        collection = self.get_collection_name(page_no, primary_db)
        filter = {"pid.page-no": page_no, "pid.frame-id": frame_id}
        result = self.db[collection].find_one_and_replace(filter, document)

        if result is None:
            result = self.db[collection].insert_one(document)

        self._remove_id(result)

        return result

    def insert_document_from_path(self, path_to_doc: str, primary_db=False):

        logger.debug("Insert document from path.")
        with open(path_to_doc, "r") as file:
            json_doc = file.read()
        document = json.loads(json_doc)

        page_no = document["pid"]["page-no"]
        frame_id = document["pid"]["frame-id"]

        collection = self.get_collection_name(page_no, primary_db)
        filter = {"pid.page-no": page_no, "pid.frame-id": frame_id}
        result = self.db[collection].find_one_and_replace(filter, document)

        if result is None:
            result = self.db[collection].insert_one(document)

        self._remove_id(result)

        return result

    def delete_frame(self, page_no: int, frame_id: chr, primary_db=False):

        logger.debug("Delete document.")
        collection = self.get_collection_name(page_no, primary_db)
        filter = {"pid.page-no": page_no, "pid.frame-id": frame_id}

        return self.db[collection].find_one_and_delete(filter)

    def get_server_status(self):

        logger.debug("Get server status.")
        return self.db.command("serverStatus")

    def create_pk_index(self):

        logger.debug("Create PK index.")
        collections = self.db.list_collection_names()
        for collection in collections:
            self._create_pk_index(collection)

    def purge_documents(self, page_no: int, frame_id, primary_db=False):

        logger.debug("Purge documents.")
        collection = self.get_collection_name(page_no, primary_db=primary_db)
        docs_deleted = 0
        while len(str(page_no)) <= 16:  # 16 for safety.

            # given a base page number we need to purge all documents
            # including zero page routing ones
            filter = {"pid.page-no": page_no, "pid.frame-id": {"$gte": frame_id}}
            result = self.db[collection].delete_many(filter)
            docs_deleted += result.deleted_count

            page_no *= 10

            # zero page routing means thatframe id is set to "a"
            frame_id = "a"

        return docs_deleted

    def publish_document(self, page_no: int, frame_id, visible_only = True):

        logger.debug("Publish documents.")
        # get doc from secondary
        sdoc = self.get_document(page_no, frame_id, None, visible_only, False)

        if sdoc is None:
            # could be an error or a deletion so get frame from primary
            pdoc = self.get_document(page_no, frame_id, None, visible_only, True)
            if pdoc is not None:
                # sdoc does not exist but pdoc does, so it needs deleting
                return self.delete_frame(page_no, frame_id, True)
            else:
                return None
        else:
            # insert in primary
            return self.insert_document(sdoc, True)

    # def publish_documents(self, include_deleted = False):
    #
    #     ("Publishing static documents from Secondary DB to Primary DB.")
    #
    #     # get collections
    #     collections = self._get_collection_names()
    #     pframes = []
    #     sframes = []
    #
    #     # iterate through the secondary database
    #     for collection in collections[0]:
    #         pframes.extend(list(self.get_all_static_documents(collection)))
    #
    #     for collection in collections[1]:
    #         sframes.extend(list(self.get_all_static_documents(collection)))
    #
    #     for frame in sframes:
    #
    #         self._remove_id(frame)
    #         pid = frame["pid"]
    #         pageId = str(pid["page-no"]) + pid["frame-id"]
    #
    #         find = next((item for item in pframes if item["pid"] == pid), None)
    #
    #         if find is not None:
    #             self._remove_id(find)
    #             #print("Frame in Primary collection: {0}".format(pid))
    #             if find != frame:
    #                 logger.info("Updating Frame [{0}].".format(pageId))
    #                 self.insert_document(frame, True)
    #             else:
    #                 logger.info("Frame at Latest Version [{0}].".format(pageId))
    #         else:
    #             #print("Frame not in Primary collection {0}".format(pid))
    #             logger.info("Inserting Frame [{0}]".format(pageId))
    #             self.insert_document(frame, True)
    #
    #     # TODO Determine those to be deleted
    #
    #         # get all static pages from primary
    #
    #
    #     # insert any in secondary that are not in primary
    #     # update any that are in secondary that are different in primary
    #
    #     # delete any in primary that are not in secondary
    #
    #     a = 0
    #     # get all static pages from secondary
    #     # get all static pages from primary
    #     # insert any in secondary that are not in primary
    #     # update any that are in secondary that are different in primary
    #     # delete any in primary that are not in secondary

    def backup_collection(self, collection, destination_directory: str):

        logger.debug("Backup collection.")
        # tidy up the directory name
        while destination_directory.endswith("/"):
            destination_directory = destination_directory[:-1]

        frames = self.get_documents_by_collection(collection)
        for frame in frames:

            try:
                # remove the auto assigned _id
                self._remove_id(frame)

                json_frame = json.dumps(frame)

                page_id = "{0}{1}".format(frame["pid"]["page-no"], frame["pid"]["frame-id"])
                # self.logger.info("{0}: Saving page {1}.".format(count, page_id))

                with open("{0}/{1}.json".format(destination_directory, page_id), "w") as file:
                    file.write(str(json_frame))

            except Exception as ex:
                self.exception.error("Exception: {0} Message: {1}".format(type(ex), str(ex)))

    def backup_database(self, destination_directory: str):

        """
        NOTE: this only backs up the pages, see _get_collection_names and associated filters for details.
        :param destination_directory:
        :return:
        """
        logger.debug("Backup database.")

        # tidy up the directory name
        while destination_directory.endswith("/"):
            destination_directory = destination_directory[:-1]

        # create the directory if needed
        self.create_directory(destination_directory)
        self.create_directory(destination_directory + "/p")
        self.create_directory(destination_directory + "/s")

        # get the collection names of all of the normal IP collections
        # this does not get collections such as system auth etc.
        collections = self._get_collection_names()

        logger.info("Backing up primary documents to {0}".format(destination_directory))
        for collection in collections[0]:
            self.backup_collection(collection, destination_directory + "/p/")

        logger.info("Backing up secondary documents to {0}".format(destination_directory))
        for collection in collections[1]:
            self.backup_collection(collection, destination_directory + "/s/")

    def _remove_id(self, result):

        if type(result) is dict and "_id" in result:
            del (result["_id"])

    def _get_collection_names(self):
        """
        :return: List with both primary and secondary collections.
        system collections such as system_auth etc are not returned.
        See the filter functions for full details.
        """
        result = []
        collections = self.db.list_collection_names()
        primary = list(filter(self._primary_collection_filter, collections))
        secondary = list(filter(self._secondary_collection_filter, collections))

        # result.append(primary)
        return (primary, secondary)

    def _create_pk_index(self, collection: str):
        return self.db[collection].create_index(
            [("pid.page-no", pymongo.ASCENDING), ("pid.frame-id", pymongo.ASCENDING)],
            unique=True)

    def _drop_pk_index(self, collection: str):
        name = "pid.page-no_1_pid.frame-id_1"

        return self.db[collection].drop_index(name)

    @staticmethod
    def create_directory(directory: str = ""):
        logger.debug("Create directory.")

        if not os.path.exists(directory):
            os.makedirs(directory)

    @staticmethod
    def get_collection_name(page_no, primary_db=False):
        """
        Return the base IP page e.g page 76545 = 765, page 99 = 000 etc.
        User pages starting 8009000 - 8009999 special cases and are always
        preceded by 8009 e.g. 800924212 = 8009242 etc.
        :param page_no:
        :return collection number:
        """
        logger.debug("Get collection name.")

        prefix = "p" if primary_db else "s"

        user_page_base = "8009"
        user_space = False

        page_nos = str(page_no)

        # user pages based around 8009 are special case
        if page_nos.startswith(user_page_base) and len(page_nos) >= 7:
            user_space = True
            page_no = int(page_nos[4:])

        # pages below 100
        if page_no < 100:
            result = "000"

        else:
            while page_no > 999:
                page_no = page_no // 10  # integer division

            result = str(page_no)

        if user_space:
            return prefix + user_page_base + result
        else:
            return prefix + result

    @staticmethod
    def _primary_collection_filter(collection: str):
        if len(collection) == 4 and collection.startswith('p'):
            return True
        else:
            return False

    @staticmethod
    def _secondary_collection_filter(collection: str):
        if len(collection) == 4 and collection.startswith('s'):
            return True
        else:
            return False
