# Frame class: this class is use as a base class for a page frame.
#
#
import json
import uuid
import pickle
import datetime
import psutil
import re

from telstar.exceptions import *
from telstar.globals import *
from telstar import frame_document
from telstar.log_utils import *

logger = getlogger(__name__)

class RenderOptions:

    def __init__(self) :
        self.show_navigation = True
        self.show_pageid = True
        self.disable_vertical_rollover = True

class Frame:
    """Viewdata Page, to be used or derived from."""

    def __init__(self, page_id: str):

        # this is used to indicate the frame type when dumping to json etc.
        # it should be overridden in sub classes.
        self.frame_type_name = None

        # must be 32 chars not including ESCs
        self.header_text = ""

        # TODO: fix this as these could get out of step
        self.page_id = page_id

        pid = self.get_pageid_elements(page_id)
        self.page_no = pid["page_no"]
        self.frame_id = pid["frame_id"]

        # Create default routing table (entries 0-9)
        self.routing_table = []
        for n in range(0, 10):
            self.routing_table.append(n + (self.page_no * 10))

        # sort out a suitable default h'hash' address entry (10) e.g. page 20112 would navigate to 201 on pressing #
        # this only applies if routing does not detect a follow on frame
        pn = self.page_no
        while pn > 999:
            pn = pn // 10
        self.routing_table.append(pn)

        # used to store any additional data e.g. gateway connection objects etc.
        self.connection = None

        # the following is used when caching page objects to file. Typically this
        # attribute would hold the database object id and is used to check for updates to the database
        self.cache_id = uuid.uuid4()

        self.cursor = False
        self.title = ""
        self.content = None
        self.has_follow_on_frame = False
        self.nav_msg = ""  # do not set the default message here, see self._nav_msg_default and _nav_next_msg_default
        self.sys_msg = ""
        self.parity_enabled = False
        self._rowNumber = 0;
        self._colNumber = 0;
        self.author_id = ""

        # defaults
        self.nav_msg_forecolour = "yellow"
        self.nav_msg_backcolour = "blue"
        self.nav_msg_highlight = "white"

        self.fields = []  # typically used for response dynamic_frames as user entry fields

    def _get_logo(self):

        default_header_logo = "\x1b\x42T\x1b\x41E\x1b\x46L\x1b\x44S\x1b\x47T\x1b\x45A\x1b\x43R          "
        max_length = 24

        if self.header_text is not None and len(self.header_text) > 0:

            logo = frame_document.decode_markup(self.header_text)

            # logo = logo.replace("[R]", k_controls["alpha_red"])
            # logo = logo.replace("[G]", k_controls["alpha_green"])
            # logo = logo.replace("[Y]", k_controls["alpha_yellow"])
            # logo = logo.replace("[B]", k_controls["alpha_blue"])
            # logo = logo.replace("[M]", k_controls["alpha_magenta"])
            # logo = logo.replace("[C]", k_controls["alpha_cyan"])
            # logo = logo.replace("[W]", k_controls["alpha_white"])
            # logo = logo.replace("[F]", k_controls["flash"])
            # logo = logo.replace("[S]", k_controls["steady"])
            # logo = logo.replace("[N]", k_controls["normal_height"])
            # logo = logo.replace("[D]", k_controls["double_height"])

            # get the actual length ESCs and all
            alen = len(logo)

            # get len not including ESCs
            count = 0
            for c in logo:
                if c != '\x1b':
                    count += 1

            if count >= max_length:  # max is 24 chars not including ESCs
                raise FrameHeaderError("Logo too long.")

            return logo.ljust(alen + (max_length - count))

        else:
            return default_header_logo

    def get_metadata(self):

        # today = datetime.date.today()
        now = datetime.datetime.utcnow()

        meta = str(now.year) + str(now.month).zfill(2) + str(now.day).zfill(2) + 'T' + str(now.hour).zfill(2) + str(
            now.minute).zfill(2) + 'Z'
        return meta

    # This method will render the page by writing the header, content and
    # footer to the stream passed in to the methods.
    # The property is overidden in sub classes.
    def render(self, buffer, hide_pageid = False, hide_navigation_message = False, navigation_row: int = 23):

        # page number starts on col 26ish
        # meta-data between cls codes
        header_meta = "\x0c{0}\x0c{1}"

        # must be 32 chars not including ESCs
        logo = self._get_logo()

        # build final header template
        header_template = header_meta + logo + k_controls["alpha_yellow"] + "{2}\r\n"
        # self._header_template = "\x0c{0}\x0c{1}\x1b\x42T\x1b\x41E\x1b\x46L\x1b\x44S\x1b\x47T\x1b\x45A\x1b\x43R          {2}{3}\r\n"

        # write to IOStream initial page is special case as no page number is displayed

        if type(self) == InitialFrame or hide_pageid:
            self.write_to_buffer(buffer,
                                 header_template.format(self.get_metadata(), '\x11' if self.cursor else "\x14",
                                                        ''))
        else:
            self.write_to_buffer(buffer, header_template.format('', '\x11' if self.cursor else "\x14",
                                                                self.page_id))

        # double height Title
        if self.title is not None and len(self.title) > 0:
            self.write_to_buffer(buffer, self.title)

        self.render_content(buffer)

        if type(self) != TestFrame:

            # render the footer
            self.vtab(buffer, 23)
            self.write_to_buffer(buffer, self._format_sys_msg(self.sys_msg))

            self._set_navigation_row(buffer, navigation_row)
            if not hide_navigation_message:
                self._render_nav(buffer)
            else:
                pass
                # TODO need to shift cursor to row specified in settings


        # if exit frame send NULL additional as a session end sequence,
        # this is used by the modem server
        if type(self) == ExitFrame:
            # three sent for good measure
            self.write_to_buffer(buffer, chr(0))
            self.write_to_buffer(buffer, chr(0))
            self.write_to_buffer(buffer, chr(0))

    def render_content(self, buffer):

        if type(self.content) is str:
            self.write_to_buffer(buffer, self.content)
        else:
            raise InvalidFrameContent("The data type {0} is not correct for frame content.", type(self.content))

    def render_partial(self, buffer, hide_navigation_message = False, navigation_row: int = 23):

        # the _sys_msg_template puts the cursor back at the Nav message line
        self.vtab(buffer, 23)
        self.write_to_buffer(buffer, self._format_sys_msg(self.sys_msg))

        self._set_navigation_row(buffer, navigation_row)
        if not hide_navigation_message:
            # clear the nav line (clumsy but better than a loop
            # max message size = 28 + 9 digit page no = 37
            self.write_to_buffer(buffer, self._format_nav_msg(''))
            self._render_nav(buffer)
        else:
            pass
            # TODO need to shift cursor to row specified in settings

    def _set_navigation_row(self, buffer, navigation_row: int):

        if (navigation_row < 1):
            navigation_row = 1
        if (navigation_row > 23 ):
            navigation_row = 23

        self.vtab(buffer, navigation_row)

    def _render_nav(self, buffer):

        # No Nav msg on gateway connect pages
        if type(self) != GatewayFrame:

            nav_msg = self._format_nav_msg('Select item or', '*page\x5f')  # Default

            if type(self) != ExitFrame:  # exit frame does not have nav msg

                if len(self.nav_msg) > 0:
                    nav_msg = self._format_nav_msg(self.nav_msg)

                elif type(self) == InitialFrame:
                    nav_msg = self._format_nav_msg('Press \x5f to continue')

                elif self.has_follow_on_frame:
                    nav_msg = self._format_nav_msg('Press \x5f or', '*page\x5f')

                self.write_to_buffer(buffer, nav_msg)

    def _format_nav_msg(self, message, highlight_message=''):

        bg = k_controls["alpha_" + self.nav_msg_backcolour]
        fg = k_controls["alpha_" + self.nav_msg_forecolour]
        hi = k_controls["alpha_" + self.nav_msg_highlight]
        nb = k_controls['new_background']

        if len(message) > 0:
            if len(highlight_message) > 0:
                formatted_msg = bg + nb + fg + message + hi + highlight_message + fg + ':' + hi
            else:
                formatted_msg = bg + nb + fg + message + ' :' + hi
        else:
            # clear the nav line
            formatted_msg = bg + nb + '                                    \r'

        return formatted_msg

    def _format_sys_msg(self, message):

        message = frame_document.decode_markup(message)
        ## the trailing \x0b is to put the cursor on the line above the system message
        formatted_msg = '{0}\r\x0b'.format(message)
        return formatted_msg

    def get_route_entry(self, n):

        if n >= 0 and n <= 10:
            return str(self.routing_table[n])
        else:
            return 0

    def set_route_entry(self, n, value):

        if value is not None and n is not None and 0 <= n <= 10:
            self.routing_table[n] = value

    def write_to_buffer(self, buffer, data):

        if data is not None and buffer is not None:

            for char in data:

                asc = ord(char)
                # this helps in not returning any received characters from TELNET negotiation
                if asc < 128:

                    # apply parity to everything
                    if self.parity_enabled:

                        buffer.append(self.apply_parity(char.encode('utf-8')))
                    else:
                        buffer.append(char.encode('utf-8'))

                    if char == '\r':
                        self._rowNumber += 1
                        self._colNumber = 0
                    elif char >= ' ':
                        self._colNumber += 1

                    # REOMOVE THIS CODE
                    # This code aded to capture a binary version of a few pages
                    # file = open("./bin/" + self.page_id + ".bin", "ab")
                    # file.write(char.encode("utf-8"))
                    # file.close

                    # if(self.page_id == '1a'):
                    #    print("{0}/{1} {2:02x} {3:c}".format(self._rowNumber, self._colNumber, asc ,0x20 if asc & 0x7f < 0x20 else asc & 0x7f))

    def position_cursor(self, buffer, horz_position, vert_position):

        self.vtab(buffer, vert_position)
        self.htab(buffer, horz_position)

    # this tabs vertically down after performing a home
    def vtab(self, buffer, target_line):

        self.write_to_buffer(buffer, '\x1e')  # Home

        # if self.settings is not None and (self.settings['port'] == '6504' or self.settings['port'] == '25232'):

        # TODO: Use this code once all of Richard Russell's clients have been updated, and remove the
        # TODO: conditional line above.

        # Use shortest route to target position i.e. down or up and over the top.
        # Disabled as this causes issues with some clients 7 March 2022
        #if target_line > 12 :
        #    for line in range(0, 24 - target_line):
        #        self.write_to_buffer(buffer, '\x0b')
        #else:
        #    for line in range(0, target_line):
        #        self.write_to_buffer(buffer, '\n')

        # this code used instead of above as it causes issues with some clients 7 March 2022
        for line in range(0, target_line):
            self.write_to_buffer(buffer, '\n')

        # else:
        #    # TODO: Remove this code once all of Richard Russell's clients have been updated.
        #    for line in range(0, target_line):
        #        self.write_to_buffer(buffer, '\n')

        self.write_to_buffer(buffer, '\r')

    # this tabs horozontally across after performing a CR (not NL)
    def htab(self, buffer, target_row):

        self.write_to_buffer(buffer, '\r')  # Home

        for line in range(0, target_row):
            self.write_to_buffer(buffer, '\x09')  # cursor forward

    def pickle(self, filename):

        # pickle the frame
        file = open(filename, "wb")
        pickle.dump(self, file)
        file.close
        # print('Frame created ({0}).'.format(filename))

    def __str__(self, partial=False):

        return self.dump()

    def load(self, json_doc: str):

        try:
            # convert to dict
            json_dict = json.loads(json_doc)
            self.load_from_dict(json_dict)

        except Exception as ex:
            logger.exception(ex)

    def load_from_dict(self, json_dict: dict):

        try:
            if json_dict is not None:
                # get the relevant properties
                # self.routing_table = json_dict["routing-table"]
                # self.routing_table self._get_disct_item(json_doc, "routing-table")
                if "header-text" in json_dict:
                    self.header_text = json_dict["header-text"]
                if "routing-table" in json_dict:
                    self.routing_table = json_dict["routing-table"]
                if "content" in json_dict:
                    c = json_dict["content"]
                    if "data" in c:
                        self.content = c["data"]
                if "title" in json_dict:
                    t = json_dict["title"]
                    if "data" in t:
                        self.title = t["data"]
                if "connection" in json_dict:
                    self.connection = json_dict["connection"]
                if "response-fields" in json_dict:
                    self.fields = json_dict["response-fields"]
                if "cursor" in json_dict:
                    self.cursor = json_dict["cursor"]
                if "author-id" in json_dict:
                    self.author_id = json_dict["author-id"]
                if "navmsg-forecolour" in json_dict:
                    self.nav_msg_forecolour = json_dict["navmsg-forecolour"]
                if "navmsg-backcolour" in json_dict:
                    self.nav_msg_backcolour = json_dict["navmsg-backcolour"]
                if "navmsg-highlight" in json_dict:
                    self.nav_msg_highlight = json_dict["navmsg-highlight"]

        except Exception as ex:
            logger.exception(ex)

        # validate the frame
        if type(self.page_no) is not int or \
                type(self.frame_id) is not str or \
                len(self.frame_id) != 1 or \
                len(self.routing_table) != 11:
            raise InvalidFrame("Frame has an invalid structure.")

    def dump(self):

        content = self.content

        # bytes objects are not serializable
        if type(self.content) is bytes:
            content = content.decode('utf-8')

        dict = {
            "pid": {"page-no": self.page_no,
                    "frame-id": self.frame_id,
                    # "frame-creation": self.creation_time
                    },
            "header-text": self.header_text,
            "_cache-id": str(self.cache_id),
            "frame-type": self.frame_type_name,
            "content": {
                "data": content,
                # always raw as the content is always stored as raw with the frame object
                "type": "raw"
            },
            "title": {
                "data": self.title,
                # always raw as the conversion from edit.tf or whatever is done in the dal
                "type": "raw"
            },
            "routing-table": self.routing_table,
            "connection": self.connection,
            "author-id": self.author_id,
            "response-fields": self.fields,
            "cursor": self.cursor,
            "navmsg-forecolour": self.nav_msg_forecolour,
            "navmsg-backcolour": self.nav_msg_backcolour,
            "navmsg-highlight": self.nav_msg_highlight,
        }
        return json.dumps(dict)

    # @staticmethod
    # def get_pageid_elements(page_id):
    #
    #     try:
    #         result = {}
    #         # result =[None, None]
    #         if 97 <= ord(page_id[-1:]) <= 122:
    #             result = {"page_no": int(page_id[:-1]), "frame_id": page_id[-1:]}
    #         else:
    #             result = {"page_no": int(page_id[:-1])}
    #
    #         return result
    #
    #     except:
    #         return None

    @staticmethod
    def get_pageid_elements(page_id):

        result = {}

        if re.match(k_re_page_id, page_id) is not None:
            result = {"page_no": int(page_id[:-1]), "frame_id": page_id[-1:]}

        elif re.match(k_re_page_no, page_id) is not None:
            result = {"page_no": int(page_id)}

        return result

    @staticmethod
    def bytes_to_string(byte_data):
        return "".join(map(chr, byte_data))

    # applies 7 bit even parity
    @staticmethod
    def apply_parity(char):

        ascii = ord(char)
        int_type = ascii

        # count the number of bits
        length = 0
        count = 0
        while (int_type):
            count += (int_type & 1)
            length += 1
            int_type >>= 1

        if count % 2 != 0:
            ascii |= 128

        char = chr(ascii).encode('utf-8')

        # print("Char: {0}, int: {1}, bits set: {2} parity: {3}".format(char, hex(ascii), count, 0 if count % 2 == 0  else 1))

        return char

    @staticmethod
    def unpickle(filename):

        try:
            file = open(filename, "rb")
            frame = pickle.load(file)
            file.close()
            return frame

        except Exception as ex:
            traceback.print_exc()

            return None

    # This method is designed to format the content into a word wrapped block with each line less than 40 chars
    # It returns a list of paragraph lines. CR, NL, or CR/NL combinations are replaced with a single space.
    @staticmethod
    def format(text, max_line_length=39):

        formatted_text = []
        lines = 0
        text_line = ""

        # replace CRs
        text = text.replace("\r", " ").replace("\n", " ")  # both exchanged for a space
        text = text.replace(' :', ':')
        text = text.replace(' .', '.')
        text = text.replace('  ', ' ')  # double spaces removed
        text = text.strip()

        # get the words
        words = text.split(" ");

        # build each line of the formatted text
        for word in words:

            # add the space before counting the length
            word += " "

            if len(text_line) + len(word) <= max_line_length:
                text_line += word
            else:
                # add the line to the formatted text
                formatted_text.append(text_line.rstrip())

                # start a new line with the word that wouldn't fit
                if len(word) <= max_line_length:
                    text_line = word
                else:
                    text_line = "<Word too long>"

                lines += 1

        # add the final word and return
        formatted_text.append(text_line.rstrip())

        return formatted_text

    # @staticmethod
    # def get_follow_on_frame_id(current_frameid):
    #
    #     # get the frame id i.e. the letter element
    #     frame_id_asc = ord(current_frameid[-1])
    #     page_id = None
    #
    #     # make sure we are a - y (not z) before looking for the next frame
    #     if 97 <= frame_id_asc < 122:  # a-z Note: less than Z
    #         frame_id_asc += 1
    #         page_id = current_frameid[:len(current_frameid) - 1] + chr(frame_id_asc)
    #
    #     return page_id

    @staticmethod
    def get_follow_on_frame_id(current_id):

        # Note that this function is different from the similarly named function in 'routing'.
        # get the frame id i.e. the letter element
        frame_id_asc = ord(current_id[-1])
        page_id = int(current_id[:-1])

        # make sure we are a - y (not z) before looking for the next frame
        if 97 <= frame_id_asc < 122:  # a-z Note: less than Z

            frame_id_asc += 1
            page_id = str(page_id) + chr(frame_id_asc)

        elif 122: # 'z'
            frame_id_asc = 97 # 'a'
            page_id = str(page_id * 10) + chr(frame_id_asc)

        return page_id

class MainIndexFrame(Frame):
    """main index e.g. 1"""

    def __init__(self, page_id):
        super().__init__(page_id)
        # this is used to indicate the frame type when dumping to json etc.
        # it should be overridden in sub classes.
        self.frame_type_name = "mainindex"


class InformationFrame(Frame):
    """normal information pages"""

    def __init__(self, page_id):
        super().__init__(page_id)
        # this is used to indicate the frame type when dumping to json etc.
        # it should be overridden in sub classes.
        self.frame_type_name = "information"


class GatewayFrame(Frame):
    """Gateway frame"""

    def __init__(self, page_id):
        super().__init__(page_id)
        # this is used to indicate the frame type when dumping to json etc.
        # it should be overridden in sub classes.
        self.frame_type_name = "gateway"


class InteractiveFrame(Frame):
    """2 panels to allow conversation type activites with word wrap used with the InteractiveFrame type"""

    def __init__(self, page_id):
        super().__init__(page_id)
        # this is used to indicate the frame type when dumping to json etc.
        # it should be overridden in sub classes.
        self.frame_type_name = "interactive"


class TestFrame(Frame):
    """Test frame"""

    def __init__(self, page_id):
        super().__init__(page_id)
        # this is used to indicate the frame type when dumping to json etc.
        # it should be overridden in sub classes.
        self.frame_type_name = "test"


class TelesoftwareFrame(Frame):

    def __init__(self, page_id):
        super().__init__(page_id)
        # this is used to indicate the frame type when dumping to json etc.
        # it should be overridden in sub classes.
        self.frame_type_name = "telesoftware"


def render(self, buffer, hide_pageid=False, hide_navigation_message=False, navigation_row: int = 23):

        # TODO: Sort out the data type for Title as per content
        # must be 32 chars not including ESCs
        logo = self._get_logo()

        # build final header template
        header_meta = "\x0c{0}\x0c{1}"
        header_template = header_meta + logo + k_controls["alpha_yellow"] + "{2}\r\n"

        if type(self) == InitialFrame or hide_pageid:
            self.write_to_buffer(buffer,
                                 header_template.format(self.get_metadata(), '\x11' if self.cursor else "\x14",
                                                        ''))
        else:
            self.write_to_buffer(buffer, header_template.format('', '\x11' if self.cursor else "\x14",
                                                                self.page_id))


        if type(self.content) is str:
            self.write_to_buffer(buffer, self.content)
        else:
            raise InvalidFrameContent("The data type should be a str type object.", type(self.content))


class ResponseFrame(Frame):
    """Resoonse frame"""

    def __init__(self, page_id):

        Frame.__init__(self, page_id)
        self.frame_type_name = "response"

        # Using a standard client signature
        self.respone_action = "OpenWeatherClient"

    def set_focus(self, buffer, field_index=0, clear_field=False):

        if len(self.fields) > 0 and type(self) == ResponseFrame:
            # set cursor in first field
            buffer.clear()

            field = self.fields[field_index]

            if field is not None:

                if clear_field:
                    # clear the field
                    # TODO: consider that by rendering the frame or cacheing the rendered frame somewhere
                    #       we could analyse the content and keep the bandwidth usage down?, perhaps ?
                    pass

                self.position_cursor(buffer, field["hpos"], field["vpos"])
                self.write_to_buffer(buffer, field["label"])

    def write_to_field(self, buffer, field_index, text):

        self.set_focus(buffer, field_index, True)
        buffer.append(text)


class SystemFrame(Frame):
    """System frame"""

    def __init__(self, page_id):

        Frame.__init__(self, page_id)
        self.frame_type_name = "system"

    def render_content(self, buffer):

        Frame.render_content(self, buffer)

        # git_id = git.Repo("../").head.object.hexsha[:7]

        cpu_count = psutil.cpu_count()

        # cpu_times = psutil.cpu_times()
        cpu_times_percent = psutil.cpu_times_percent()
        cpu_percent = psutil.cpu_percent()
        cpu_freq = psutil.cpu_freq()
        vir_memory = psutil.virtual_memory()

        col = k_controls["alpha_green"]
        colh = k_controls["alpha_white"]

        if cpu_count:
            self.write_to_buffer(buffer, '{0}CPU Count:      {1}{2}\r\n\r\n'.format(col, colh, cpu_count))

        if cpu_percent:
            self.write_to_buffer(buffer, '{0}CPU Usage (%):  {1}{2:2.2f}\r\n'.format(col, colh, cpu_percent))

        if cpu_times_percent:
            self.write_to_buffer(buffer, '  {0}User:         {1}{2:2.2f}\r\n'.format(col, colh, cpu_times_percent.user))
            self.write_to_buffer(buffer, '  {0}Nice:         {1}{2:2.2f}\r\n'.format(col, colh, cpu_times_percent.nice))
            self.write_to_buffer(buffer, '  {0}System:       {1}{2:2.2f}\r\n'.format(col, colh, cpu_times_percent.system))
            self.write_to_buffer(buffer, '  {0}Idle:         {1}{2:2.2f}\r\n\r\n'.format(col, colh, cpu_times_percent.idle))

        if cpu_freq:
            self.write_to_buffer(buffer, '{0}CPU Freq (Mhz): {1}{2}\r\n\r\n'.format(col, colh, cpu_freq.current))

        if vir_memory:
            self.write_to_buffer(buffer, '{0}Memory (KBytes):{1}{2:2.2f}\r\n'.format(col, colh, vir_memory.total / 1000))
            self.write_to_buffer(
                buffer, '  {0}Available:    {1}{2:2.2f} ({3:0.2f}%)\r\n'.format(col, colh, vir_memory.available / 1000,
                                                                            100 - vir_memory.percent))
            self.write_to_buffer(
                buffer, '  {0}Used:         {1}{2:2.2f} ({3:0.2f}%)\r\n'.format(col, colh, vir_memory.used / 1000,
                                                                            vir_memory.percent))
            self.write_to_buffer(
                buffer, '  {0}Free:         {1}{2:2.2f}\r\n'.format(col, colh, vir_memory.free / 1000))
            self.write_to_buffer(
                buffer, '  {0}Active:       {1}{2:2.2f}\r\n\r\n'.format(col, colh, vir_memory.active / 1000))
            # self.write_to_buffer(
            #     buffer, '{0}Software Build: {1}{2}\r\n'.format(col, colh, git_id))

class ExitFrame(Frame):
    """Exit frame"""

    def __init__(self, page_id):
        super().__init__(page_id)
        # this is used to indicate the frame type when dumping to json etc.
        # it should be overridden in sub classes.
        self.frame_type_name = "exit"


class InitialFrame(Frame):
    """initial page e.g. 0"""

    def __init__(self, page_id, ):
        super().__init__(page_id)
        # this is used to indicate the frame type when dumping to json etc.
        # it should be overridden in sub classes.
        self.frame_type_name = "initial"


class ExceptionFrame(Frame):

    # Just a populated information frame
    def __init__(self, page_id, ):

        super().__init__(page_id)
        self.frame_type_name = "exception"

        self.title = '{0}Service Error\r\n'.format(k_controls["double_height"])
        self.content = '\r\n\r\n\r\n\r\n'
        self.content += frame_document.centre_text('{0}This service is unable to\r\n'.format(k_controls["alpha_yellow"]))
        self.content += frame_document.centre_text('{0}process the request.\r\n\r\n'.format(k_controls["alpha_yellow"]))
        self.content += '' # adds a CR without affecting the centre formatter
        self.content += frame_document.centre_text('{0}Please try again later.\r\n'.format(k_controls["alpha_white"]))


# class LoginFrame(Frame):
#     def __init__(self, page_id, ):
#         super().__init__(page_id)
#         # this is used to indicate the frame type when dumping to json etc.
#         # it should be overridden in sub classes.
#         self.frame_type_name = "login"
#         self._authenticated = False
